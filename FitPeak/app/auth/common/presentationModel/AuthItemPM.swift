import UIKit

enum UserInputType: Int {
    case firstName
    case lastName
    case email
    case password
    case newPassword
    case unitSystem
    
    init?(index: Int) {
        switch index {
        case 0: self = .firstName
        case 1: self = .lastName
        case 2: self = .email
        case 3: self = .password
        case 4: self = .newPassword
        case 5: self = .unitSystem
        default: return nil
        }
    }
    
    var key: String {
        switch self {
        case .firstName: return "firstName"
        case .lastName: return "lastName"
        case .email: return "email"
        case .password: return "password"
        case .unitSystem: return "unitSystem"
        default: return ""
        }
    }
}

struct AuthItemPM {
    let type: UserInputType
    var keyboardType: UIKeyboardType = .default
    var autocapitalizationType: UITextAutocapitalizationType = .none
    var secureTextEntry: Bool = false
    var placeholder: String = ""
    var icon: UIImage = UIImage()
    var detailLabel: String?
    var validator: UserInputValidator?
    
    init(type: UserInputType) {
        self.type = type
        switch type {
        case .firstName:
            keyboardType = .alphabet
            placeholder = "First Name"
            autocapitalizationType = .words
            icon = UIImage(named: "person") ?? UIImage()
            validator = EmptyValidator()
        case .lastName:
            keyboardType = .alphabet
            placeholder = "Last Name"
            autocapitalizationType = .words
            icon = UIImage(named: "person") ?? UIImage()
            validator = EmptyValidator()
        case .email:
            keyboardType = .emailAddress
            placeholder = "Email"
            icon = UIImage(named: "email") ?? UIImage()
            validator = EmailValidator()
        case .password:
            secureTextEntry = true
            placeholder = "Password"
            icon = UIImage(named: "password") ?? UIImage()
            validator = PasswordValidator()
        case .newPassword:
            secureTextEntry = true
            placeholder = "Password"
            icon = UIImage(named: "password") ?? UIImage()
            detailLabel = "* 7 Characters or more"
            validator = PasswordValidator()
        default: break
        }
        
    }
    
}

protocol UserInputValidator {
    func isValid(_ input: String) -> Bool
}

struct EmptyValidator: UserInputValidator {
    func isValid(_ input: String) -> Bool {
        return !input.isEmpty
    }
}

struct EmailValidator: UserInputValidator {
    func isValid(_ input: String) -> Bool {
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex)
        return predicate.evaluate(with: input)
    }
}

struct PasswordValidator: UserInputValidator {
    func isValid(_ input: String) -> Bool {
        return input.count >= 7
    }
}
