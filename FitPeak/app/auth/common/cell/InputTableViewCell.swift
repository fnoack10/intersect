import UIKit

protocol InputTableViewCellDelegate: class {
    func didChangeInput(_ input: String, type: UserInputType)
}

class InputTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var separationView: UIView!
    
    weak var delegate: InputTableViewCellDelegate?
    
    private var type: UserInputType?
    private var validator: UserInputValidator?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        applyTheme()
        setupUI()
    }
    
    private func applyTheme() {
        Theme.apply(self)
    }
    
    private func setupUI() {
        inputField.delegate = self
    }
    
    func configure(with presentationModel: AuthItemPM, delegate: InputTableViewCellDelegate) {
        self.delegate = delegate
        type = presentationModel.type
        validator = presentationModel.validator
        iconImage.image = presentationModel.icon.withRenderingMode(.alwaysTemplate)
        inputField.autocapitalizationType = presentationModel.autocapitalizationType
        inputField.isSecureTextEntry = presentationModel.secureTextEntry
        inputField.keyboardType = presentationModel.keyboardType
        inputField.placeholder = presentationModel.placeholder
        inputField.autocorrectionType = .no
        inputField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        detailLabel.text = presentationModel.detailLabel
    }

    // MARK - Text Field
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateEditingState(textField, active: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateEditingState(textField, active: false)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let input = textField.text, let type = type, let delegate = delegate else { return }
        delegate.didChangeInput(input, type: type)
        updateValidation(for: input)
    }
    
    func updateEditingState(_ textField: UITextField, active: Bool) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.separationView.backgroundColor = (active) ? .activeInputColor : .inactiveInputColor
        }, completion: nil)
    }
    
    func updateValidation(for input: String) {
        guard let validator = validator else { return }
        let isValid = validator.isValid(input)
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.iconImage.tintColor = isValid ? .activeInputColor : .inactiveInputColor
        }, completion: nil)
    }
}


