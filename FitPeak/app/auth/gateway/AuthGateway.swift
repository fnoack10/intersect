import Foundation
import Firebase
import RxSwift

protocol AuthGate {
    var isAuthenticated: Bool { get }
    func loginUser(with authentication: UserAuthentication) -> ResultStream<Void>
    func signupUser(with userSignup: UserSignup) -> ResultStream<Void>
}

final class AuthGateway: AuthGate {
    // MARK - Private Properties
    private var disposeBag = DisposeBag()
    private var database: Firestore!
    
    // MARK - Lifecycle
    
    init() {
        setupDatabase()
    }
    
    private func setupDatabase() {
        let settings = FirestoreSettings()
        Firestore.firestore().settings = settings
        database = Firestore.firestore()
    }
    
    // MARK - Authentication
    
    var isAuthenticated: Bool {
        return Auth.auth().currentUser != nil
    }
    
    // MARK: - Login
    
    func loginUser(with authentication: UserAuthentication) -> ResultStream<Void> {
        return Observable.create { observer -> Disposable in
            Auth.auth().signIn(withEmail: authentication.email, password: authentication.password) { (_, error) in
                if let error = error {
                    observer.onNext(.failure(error))
                } else {
                    observer.onNext(.success(()))
                }
            }
            return Disposables.create()
        }
    }
    
    // MARK: - Signup
    
    func signupUser(with userSignup: UserSignup) -> ResultStream<Void> {
        return Observable.create { observer -> Disposable in
            Auth.auth().createUser(withEmail: userSignup.email, password: userSignup.password) { (auth, error) in
                if let error = error {
                    observer.onNext(.failure(error))
                } else {
                    
                    // Add display name to account
                    guard let user = auth?.user else { return }
                    let changeRequest = user.createProfileChangeRequest()
                    changeRequest.displayName = userSignup.firstName + " " + userSignup.lastName
                    changeRequest.commitChanges { (error) in
                        if let error = error {
                            observer.onNext(.failure(error))
                        } else  {
                            
                            // Add user object to database
                            let ref: DocumentReference = Firestore.firestore().document("users/\(user.uid)")
                            ref.setData(userSignup.export()) { error in
                                if let error = error {
                                    observer.onNext(.failure(error))
                                } else  {
                                    observer.onNext(.success(()))
                                }
                            }
                        }
                    }
                }
            }
            return Disposables.create()
        }
    }
    
}

