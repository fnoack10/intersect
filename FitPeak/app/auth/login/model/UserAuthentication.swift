import Foundation

struct UserAuthentication {
    var email: String = ""
    var password: String = ""
    
    var isReadyToLogin: Bool {
        return isValidEmail && isValidPassword
    }
    
    var isValidEmail: Bool {
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex)
        return predicate.evaluate(with: email)
    }
    
    var isValidPassword: Bool {
        return password.count >= 7
    }
}
