import Foundation
import RxSwift
import RxCocoa

protocol LoginInteractorInputs {
    func configure(with presentationModel: LoginPM)
    func bind(actions: LoginUI.Actions)
}

protocol LoginInteractorOutputs: InteractorOutputs {
    var login: PublishSubject<Void> { get }
    var signup: PublishSubject<Void> { get }
}

protocol LoginInteractorUIOutputs {
    var updateUI: Driver<LoginPM> { get }
}

protocol LoginType: Interactor {
    var inputs: LoginInteractorInputs { get }
    var outputs: LoginInteractorOutputs { get }
    var uiOutputs: LoginInteractorUIOutputs { get }
}

// MARK: - Interactor

final class LoginInteractor: LoginType, LoginInteractorInputs, LoginInteractorOutputs, LoginInteractorUIOutputs {
    
    // MARK: - Inputs
    func configure(with presentationModel: LoginPM) {
        presentationModelSubject.accept(presentationModel)
    }
    
    func bind(actions: LoginUI.Actions) {
        actions.login.asObservable()
            .flatMap { self.gateway.loginUser(with: $0) }
            .subscribe(onNext: { result in
                switch result {
                case .success:
                    self.login.onCompleted()
                case let .failure(error):
                    self.updatePresentationModelWithError(error)
                }
            }).disposed(by: disposeBag)
        
        actions.signup.asObservable()
            .bind(to: signup)
            .disposed(by: disposeBag)
    }
    
    // MARK: - UIOutputs
    lazy var updateUI: Driver<LoginPM> = self.createUpdateUIStream()
    
    // MARK: - Scene Outputs
    var login = PublishSubject<Void>()
    var signup = PublishSubject<Void>()
    var didFinish = PublishSubject<Void>()
    
    // MARK: - Input Output
    var inputs: LoginInteractorInputs { return self }
    var outputs: LoginInteractorOutputs { return self }
    var uiOutputs: LoginInteractorUIOutputs { return self }
    
    // MARK: - Properties
    let disposeBag = DisposeBag()
    
    // MARK: - Private Properties
    private let gateway: AuthGate
    private let presentationModelSubject = BehaviorRelay(value: LoginPM())
    
    // MARK: - Lifecycle
    init(gateway: AuthGate) {
        self.gateway = gateway
    }
    
    // MARK: - UI Output Streams
    
    private func createUpdateUIStream() -> Driver<LoginPM> {
        return presentationModelSubject.asDriver()
    }
    
    private func updatePresentationModelWithError(_ error: Error) {
        var currentPM = self.presentationModelSubject.value
        let failurePM = currentPM.failure(error)
        self.presentationModelSubject.accept(failurePM)
    }
    
}
