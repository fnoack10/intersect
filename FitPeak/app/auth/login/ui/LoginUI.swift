import Foundation
import UIKit
import RxSwift
import RxCocoa

class LoginUI: InteractableUI<LoginInteractor>, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var disposedBag = DisposeBag()
    
    private var loginSubject = PublishSubject<UserAuthentication>()
    
    private var presentationModel = LoginPM()
    private var userAuthentication = UserAuthentication()
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var appTitleLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupLabel: UILabel!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyTheme()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    // MARK: - Setup
    private func applyTheme() {
        LoginTheme.apply(self)
        LoginTheme.apply(loginButton)
        LoginTheme.apply(signupLabel)
        LoginTheme.apply(welcomeLabel: welcomeLabel)
        LoginTheme.apply(appTitleLabel: appTitleLabel)
        LoginTheme.apply(signupButton: signupButton)
    }
    
    private func setupUI() {
        setupTableView()
        setupKeyboard()
        setupActions()
        setupActivityIndicator()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = presentationModel.rowHeight
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func setupActions() {
        loginButton.rx.tap.asObservable()
            .map { self.userAuthentication }
            .bind(to: loginSubject)
            .disposed(by: disposeBag)
        
        loginButton.rx.tap.asObservable()
            .subscribe(onNext: { _ in
                self.startLoading()
            }).disposed(by: disposeBag)
    }
    
    private func setupActivityIndicator() {
        activityIndicator.hidesWhenStopped = true
    }

    // MARK: - Bind Interactor
    override func bindInteractor(interactor: LoginInteractor) {
        interactor.uiOutputs.updateUI.drive(onNext: { [weak self] presentationModel in
            self?.presentationModel = presentationModel
            self?.updateUI(presentationModel)
        }).disposed(by: disposedBag)
    }
    
    private func updateUI(_ presentationModel: LoginPM) {
        switch presentationModel.presentationState {
        case .success:
            welcomeLabel.text = presentationModel.welcome
            appTitleLabel.text = presentationModel.appName
            loginButton.setTitle(presentationModel.loginTitle, for: .normal)
            signupLabel.text = presentationModel.signUpQuestion
            signupButton.setTitle(presentationModel.signUpAnswer, for: .normal)
        case .failure(let error):
            endLoading()
            ErrorHandler.showError(error, delegate: self)
        default: break
        }
    }
    
    // MARK - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return presentationModel.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentationModel.sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item: AuthItemPM = presentationModel.sections[indexPath.section].items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: presentationModel.cellIdentifier, for: indexPath) as! InputTableViewCell
        
        cell.inputField.delegate = self
        cell.inputField.tag = indexPath.row
        cell.inputField.autocorrectionType = .no
        cell.inputField.keyboardType = item.keyboardType
        cell.inputField.isSecureTextEntry = item.secureTextEntry
        cell.inputField.placeholder = item.placeholder
        cell.inputField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        cell.iconImage.image = item.icon.withRenderingMode(.alwaysTemplate)
        cell.detailLabel.text = item.detailLabel
        cell.separationView.backgroundColor = .separationLine
        
        return cell
    }
    
    // MARK - Text Field
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateEditingState(textField, active: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateEditingState(textField, active: false)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let type = presentationModel.sections[0].items[textField.tag].type
        guard let input = textField.text else { return }
        
        switch type {
        case .email:
            userAuthentication.email = input
        case .password:
            userAuthentication.password = input
        default: break
        }
        
        updateInputValidation(textField)
    }
    
    // MARK - Keyboard
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.height {
                self.bottomConstraint.constant = keyboardHeight/2 + 10
                self.view.layoutIfNeeded()
            }
        }, completion: nil)
    }
    
    @objc private func keyboardWillHide(_ notification: NSNotification) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.bottomConstraint.constant = 20
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // MARK - Helper Methods
    func updateEditingState(_ textField: UITextField, active: Bool) {
        let index = IndexPath(row: textField.tag, section: 0)
        let cell = tableView.cellForRow(at: index) as! InputTableViewCell
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            cell.separationView.backgroundColor = (active) ? .activeInputColor : .inactiveInputColor
        }, completion: nil)
    }
    
    func updateInputValidation(_ textField: UITextField) {
        let type = presentationModel.sections[0].items[textField.tag].type
        let index = IndexPath(row: textField.tag, section: 0)
        let cell = tableView.cellForRow(at: index) as! InputTableViewCell
        
        var validation: Bool
        switch type {
        case .email:
            validation = userAuthentication.isValidEmail
        case .password:
            validation = userAuthentication.isValidPassword
        default:
            validation = false
        }
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            cell.iconImage.tintColor = validation ? .activeInputColor : .inactiveInputColor
        }, completion: nil)
    }
    
    // MARK - Activity
    private func startLoading() {
        activityIndicator.startAnimating()
        loginButton.isEnabled = false
        signupButton.isEnabled = false
    }
    
    private func endLoading() {
        activityIndicator.stopAnimating()
        loginButton.isEnabled = true
        signupButton.isEnabled = true
    }
    
}

// MARK: UI Actions
extension LoginUI {
    struct Actions {
        let login: Driver<UserAuthentication>
        let signup: Driver<Void>
    }
    
    var actions: Actions {
        return Actions(
            login: loginSubject.asDriver(onErrorJustReturn: UserAuthentication()),
            signup: signupButton.rx.tap.asDriver(onErrorJustReturn: ())
        )
    }
}

