import UIKit

struct LoginTheme: ThemeProtocol {
    
    static func apply(welcomeLabel: UILabel) {
        welcomeLabel.font = UIFont(.bold, 24)
        welcomeLabel.textColor = .primaryTextColor
    }
    
    static func apply(appTitleLabel: UILabel) {
        appTitleLabel.font = UIFont(.bold, 50)
        appTitleLabel.textColor = .primaryTextColor
    }
    
    static func apply(signupButton: UIButton) {
        signupButton.titleLabel?.font = UIFont(.bold, 16)
        signupButton.setTitleColor(.black, for: .normal)
    }
    
}
