import Foundation
import UIKit

enum LoginError: LocalizedError {
    case invalidInput
    
    public var errorDescription: String? {
        switch self {
        case .invalidInput: return "Invalid email or password. Please try again."
        }
    }
}

struct LoginPM: PresentationModel {
    var presentationState: PresentationState = .success
    let welcome: String = "Welcome to"
    let appName: String = "FitPeak"
    let loginTitle: String = "Login"
    let signUpAnswer: String = "Sign Up"
    let signUpQuestion: String = "Are you new?"
    let cellIdentifier: String = "InputTableViewCell"
    let sections: [LoginSectionPM] = [LoginSectionPM()]
    let rowHeight: CGFloat = 60.0
}
