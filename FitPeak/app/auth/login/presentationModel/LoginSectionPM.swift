import Foundation

struct LoginSectionPM {
    var items: [AuthItemPM] = [AuthItemPM(type: .email),
                               AuthItemPM(type: .password)]
}
