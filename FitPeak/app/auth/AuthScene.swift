import Foundation
import RxSwift

final class AuthScene: Scene {
    
    // MARK: - Properties
    var didFinish = PublishSubject<Void>()
    var children: [Scene] = []
    var disposeBag = DisposeBag()
    
    // MARK: - Private Properties
    private let gateway: AuthGate
    private let navigationContext: NavigationControllerContext
    
    // MARK: - Lifecycle
    init(gateway: AuthGate, context: NavigationControllerContext) {
        self.gateway = gateway
        navigationContext = context
    }
    
    func start() {
        launchLoginUI()
    }
    
}

// MARK: - UI
extension AuthScene {
    
    private func launchLoginUI() {
        let ui = LoginUI.create(scene: StoryboardScene.Login.loginUI) { loginUI in
            // This should produce exactly one type of UI
            guard let loginUI = loginUI as? LoginUI else { fatalError() }
            
            let interactor = LoginInteractor(gateway: self.gateway)
            interactor.inputs.bind(actions: loginUI.actions)
            interactor.inputs.configure(with: LoginPM())
            
            interactor.outputs.login
                .bind(to: self.didFinish)
                .disposed(by: loginUI.disposeBag)
            
            interactor.outputs.signup
                .subscribe(onNext: { [weak self] _ in
                    self?.launchSignUpUI()
                }).disposed(by: loginUI.disposeBag)
            
            return interactor
            
        }
        navigationContext.push(ui, animated: false, mode: .topmost)
    }
    
    private func launchSignUpUI() {
        let ui = SignUpUI.create(scene: StoryboardScene.Signup.signUpUI) { signUpUI in
            // This should produce exactly one type of UI
            guard let signUpUI = signUpUI as? SignUpUI else { fatalError() }
            
            let interactor = SignUpInteractor()
            interactor.inputs.bind(actions: signUpUI.actions)
            
            interactor.outputs.submit
                .subscribe(onNext: { [weak self] userSignup in
                    self?.launchMeasurementUI(with: userSignup)
                }).disposed(by: signUpUI.disposeBag)
            
            interactor.outputs.back
                .subscribe(onNext: { [weak self] _ in
                    self?.navigationContext.pop(animated: true)
                }).disposed(by: signUpUI.disposeBag)
            
            return interactor
        }
        navigationContext.push(ui)
    }
    
    private func launchMeasurementUI(with userSignup: UserSignup) {
        let ui = MeasurementUI.create(scene: StoryboardScene.Signup.measurementUI) { measurementUI in
            // This should produce exactly one type of UI
            guard let measurementUI = measurementUI as? MeasurementUI else { fatalError() }
            
            let interactor = MeasurementInteractor(gateway: self.gateway, userSignup: userSignup)
            interactor.inputs.bind(actions: measurementUI.actions)
        
            interactor.outputs.signupCompleted
                .subscribe(onNext: { [weak self] userSignup in
                    self?.didFinish.onCompleted()
                }).disposed(by: measurementUI.disposeBag)
            
            interactor.outputs.back
                .subscribe(onNext: { [weak self] _ in
                    self?.navigationContext.pop(animated: true)
                }).disposed(by: measurementUI.disposeBag)

            return interactor
        }
        navigationContext.push(ui)
    }
    
}
