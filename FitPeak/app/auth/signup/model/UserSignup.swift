import Foundation
import UIKit

struct UserSignup {
    var firstName: String = ""
    var lastName: String = ""
    var email: String = ""
    var password: String = ""
    var unitSystem: UnitSystem?
    
    var displayName: String {
        return firstName + " " + lastName
    }
    
    var isReadyToSignUp: Bool {
        return  isValidFirstName && isValidLastName && isValidEmail && isValidPassword
    }
    
    var isValidFirstName: Bool {
        return !firstName.isEmpty
    }
    
    var isValidLastName: Bool {
        return !lastName.isEmpty
    }
    
    var isValidEmail: Bool {
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex)
        return predicate.evaluate(with: email)
    }
    
    var isValidPassword: Bool {
        return password.count >= 7
    }
    
    mutating func update(_ input: String ,of type: UserInputType) {
        switch type {
        case .firstName: firstName = input
        case .lastName: lastName = input
        case .email: email = input
        case .password: password = input
        case .newPassword: password = input
        default: break
        }
    }
    
    func validation(for type: UserInputType) -> Bool {
        switch type {
        case .firstName: return isValidFirstName
        case .lastName: return isValidLastName
        case .email: return isValidEmail
        case .password, .newPassword: return isValidPassword
        default: return false
        }
    }
    
    func export() -> [String: Any] {
        guard let unitSystem = self.unitSystem else { return [:] }
        return [ UserInputType.firstName.key: self.firstName,
                 UserInputType.lastName.key: self.lastName,
                 UserInputType.email.key: self.email,
                 UserInputType.unitSystem.key: unitSystem.rawValue]
    }
}
