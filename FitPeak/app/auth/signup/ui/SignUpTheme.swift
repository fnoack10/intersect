import UIKit

struct SignUpTheme: ThemeProtocol {
    
    static func apply(selectionButton: UIButton) {
        selectionButton.titleLabel?.font = UIFont(.bold, 16)
        selectionButton.tintColor = .clear
        selectionButton.setTitleColor(.activeTextColor, for: .normal)
        selectionButton.setTitleColor(.activeTextColor, for: .highlighted)
        selectionButton.setTitleColor(.lightTextColor, for: .selected)
        selectionButton.setBackgroundColor(color: .backgroundColorPrimary, forState: .normal)
        selectionButton.setBackgroundColor(color: .backgroundColorPrimary, forState: .highlighted)
        selectionButton.setBackgroundColor(color: .activeButtonColor, forState: .selected)
        selectionButton.layer.cornerRadius = 12.0
        selectionButton.layer.borderWidth = 1
        selectionButton.layer.borderColor = UIColor.activeButtonColor.cgColor
        selectionButton.clipsToBounds = true
    }
    
    static func apply(signupButton: UIButton) {
        signupButton.titleLabel?.font = UIFont(.bold, 16)
        signupButton.setTitleColor(.black, for: .normal)
    }
    
}
