import Foundation
import UIKit
import RxSwift
import RxCocoa
import FirebaseAuth
import FirebaseFirestore

class SignUpUI: InteractableUI<SignUpInteractor>, UITableViewDelegate, UITableViewDataSource, InputTableViewCellDelegate {
    
    var updateInputSubject = PublishSubject<(String, UserInputType)?>()
    var disposedBag = DisposeBag()
    
    // MARK: - Outlets
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    private lazy var backButtonItem: UIBarButtonItem = self.createLeftBarButtonItem()

    // MARK: - Private Properties
    private var presentationModel: SignUpPM?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        applyTheme()
        setupUI()
    }
    
    private func applyTheme() {
        SignUpTheme.apply(self)
        SignUpTheme.apply(navigationController, tintColor: .navigationTintDark)
        SignUpTheme.apply(continueButton)
    }
    
    private func setupUI() {
        setupNavigationUI()
        setupTableView()
        setupKeyboard()
        setupGestures()
        setupScrollView()
    }
    
    private func setupNavigationUI() {
        navigationItem.leftBarButtonItem = backButtonItem
    }
    
    private func createLeftBarButtonItem() -> UIBarButtonItem {
        let backItem: NavigationItemType = .back
        let backButtonItem = UIBarButtonItem(image: backItem.icon, style: .plain, target: nil, action: nil)
        backButtonItem.tintColor = .navigationTintDark
        return backButtonItem
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 60, right: 0)
    }
    
    private func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func setupGestures() {
        let tapRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    private func setupScrollView() {
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    // MARK: - Bind Interactor
    override func bindInteractor(interactor: SignUpInteractor) {
        interactor.uiOutputs.updateUI.drive(onNext: { [weak self] presentationModel in
            self?.presentationModel = presentationModel
            self?.updateUI(presentationModel)
        }).disposed(by: disposedBag)
    }
    
    private func updateUI(_ presentationModel: SignUpPM) {
        title = presentationModel.title
        continueButton.setTitle(presentationModel.continueButtonTitle, for: .normal)
        continueButton.isEnabled = presentationModel.isContinueButtonEnabled
        
        if presentationModel.shouldReload {
            tableView.reloadData()
        }
    }
    
    // MARK - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return presentationModel?.sections.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentationModel?.sections[section].items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return presentationModel?.headerHeight ?? CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // TODO - UUUGHHHHH! Refactor this
        guard let presentationModel = presentationModel else { return UIView() }
        let sectionItem = presentationModel.sections[section]
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: presentationModel.headerHeight))
        headerView.backgroundColor = .backgroundColorPrimary
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 20, width: view.frame.width, height: presentationModel.headerHeight-20))
        headerLabel.font = UIFont(.bold, 12)
        headerLabel.textColor = .gray
        headerLabel.text = sectionItem.title.uppercased()
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let presentationModel = presentationModel else { return UITableViewCell() }
        let itemPM: AuthItemPM = presentationModel.sections[indexPath.section].items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: presentationModel.cellIdentifier, for: indexPath) as! InputTableViewCell
        cell.configure(with: itemPM, delegate: self)
        return cell
    }
    
    // MARK - Cell Delegate
    func didChangeInput(_ input: String, type: UserInputType) {
        updateInputSubject.onNext((input, type))
    }
    
    // MARK - Keyboard
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.height {
                self.tableViewBottomConstraint.constant = keyboardHeight
                self.view.layoutIfNeeded()
            }
        }, completion: nil)
    }
    
    @objc private func keyboardWillHide(_ notification: NSNotification) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.tableViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
}

// MARK: UI Actions
extension SignUpUI {
    struct Actions {
        let submit: Driver<Void>
        let updateInput: Driver<(String, UserInputType)?>
        let back: Driver<Void>
    }
    
    var actions: Actions {
        return Actions(
            submit: continueButton.rx.tap.asDriver(onErrorJustReturn: ()),
            updateInput: updateInputSubject.asDriver(onErrorJustReturn: nil),
            back: backButtonItem.rx.tap.asDriver(onErrorJustReturn: ())
        )
    }
}
