import Foundation
import UIKit
import RxSwift
import RxCocoa
import FirebaseAuth
import FirebaseFirestore

class MeasurementUI: InteractableUI<MeasurementType> {
    
    var measurementSubject = PublishSubject<UnitSystem?>()
    var backSubject = PublishSubject<Void>()
    var disposedBag = DisposeBag()
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var metricButton: UIButton!
    @IBOutlet weak var englishButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        setupNavigationUI()
        setupActions()
        applyTheme()
    }
    
    private func applyTheme() {
        SignUpTheme.apply(self)
        SignUpTheme.apply(navigationController, tintColor: .navigationTintDark)
        SignUpTheme.apply(descriptionLabel)
        SignUpTheme.apply(selectionButton: metricButton)
        SignUpTheme.apply(selectionButton: englishButton)
        SignUpTheme.apply(signupButton)
    }
    
    private func setupNavigationUI() {
        let backItem: NavigationItemType = .back
        let backButtonItem = UIBarButtonItem(image: backItem.icon, style: .plain, target: self, action: #selector(backAction))
        backButtonItem.tintColor = .navigationTintDark
        navigationItem.leftBarButtonItem = backButtonItem
    }
    
    private func setupActions() {
        signupButton.rx.tap.asObservable()
            .subscribe(onNext: { _ in
                self.startLoading()
            }).disposed(by: disposeBag)
    }
    
    // MARK: - Bind Interactor
    
    override func bindInteractor(interactor: MeasurementType) {
        interactor.uiOutputs.updateUI.drive(onNext: { [weak self] presentationModel in
            self?.updateUI(presentationModel)
        }).disposed(by: disposedBag)
    }
    
    private func updateUI(_ presentationModel: MeasurementPM) {
        title = presentationModel.title
    
        descriptionLabel.text = presentationModel.description
        metricButton.setTitle(UnitSystem.metric.title , for: .normal)
        englishButton.setTitle(UnitSystem.english.title, for: .normal)
        signupButton.setTitle(presentationModel.buttonTitle, for: .normal)
        metricButton.isSelected = presentationModel.isMetricButtonSelected
        englishButton.isSelected = presentationModel.isEnglishButtonSelected
        signupButton.isEnabled = presentationModel.isSignupButtonEnabled
        activityIndicator.hidesWhenStopped = true
        endLoading()
    }
    
    // MARK - Actions
    
    @IBAction func metricAction(_ sender: Any) {
        measurementSubject.onNext(.metric)
    }
    
    @IBAction func englishAction(_ sender: Any) {
        measurementSubject.onNext(.english)
    }
    
    @objc private func backAction() {
        backSubject.onNext(())
    }
     
    // MARK - Activity
    
    private func startLoading() {
        activityIndicator.startAnimating()
        signupButton.isEnabled = false
    }
    
    private func endLoading() {
        activityIndicator.stopAnimating()
        signupButton.isEnabled = true
    }
}

// MARK: UI Actions
extension MeasurementUI {
    struct Actions {
        let signup: Driver<Void>
        let selectedMeasurment: Driver<UnitSystem?>
        let back: Driver<Void>
    }
    
    var actions: Actions {
        return Actions(
            signup: signupButton.rx.tap.asDriver(onErrorJustReturn: ()),
            selectedMeasurment: measurementSubject.asDriver(onErrorJustReturn: nil),
            back: backSubject.asDriver(onErrorJustReturn: ())
        )
    }
}

