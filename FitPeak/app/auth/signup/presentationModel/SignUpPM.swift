import Foundation
import UIKit

struct SignUpPM {
    let title: String = "Sign Up"
    let continueButtonTitle: String = "Continue"
    let cellIdentifier: String = "InputTableViewCell"
    let sections: [SignUpSectionPM] = [SignUpSectionPM()]
    let headerHeight: CGFloat = 50.0
    var isContinueButtonEnabled: Bool =  false
    var shouldReload: Bool = true
    
    init() {}
    
    init(_ canContinue: Bool) {
        self.shouldReload = false
        self.isContinueButtonEnabled = canContinue
    }
}
