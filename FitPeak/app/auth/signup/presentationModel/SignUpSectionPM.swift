import Foundation

struct SignUpSectionPM {
    
    let title: String
    var items: [AuthItemPM]
    
    init() {
        title = "Personal Information"
        items = [AuthItemPM(type: .firstName),
                 AuthItemPM(type: .lastName),
                 AuthItemPM(type: .email),
                 AuthItemPM(type: .newPassword)]
    }

}
