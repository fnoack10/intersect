import Foundation
import UIKit

struct MeasurementPM {
    let title: String
    let description: String
    let buttonTitle: String
    var selectedMeasurment: UnitSystem?
    var presentationState: PresentationState = .success
    
    init(selectedMeasurment: UnitSystem? = nil) {
        self.title = "Units of Measurement"
        self.description = "Which system of units would you like to use?"
        self.buttonTitle = "Sign Up"
        self.selectedMeasurment = selectedMeasurment
    }
    
    var isMetricButtonSelected: Bool {
        guard let measurment = selectedMeasurment else { return false }
        return measurment == .metric
    }
    
    var isEnglishButtonSelected: Bool {
        guard let measurment = selectedMeasurment else { return false }
        return measurment == .english
    }
    
    var isSignupButtonEnabled: Bool { selectedMeasurment != nil }
    
}
