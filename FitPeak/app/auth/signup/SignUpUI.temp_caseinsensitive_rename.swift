//
//  SignInVC.swift
//  Intersect
//
//  Created by Franco Noack on 27.12.18.
//  Copyright © 2018 Intersect. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import FirebaseAuth

class SignUpUI: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var didFinish = PublishSubject<Void>()
    var disposedBag = DisposeBag()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var signupButtonBottomConstraint: NSLayoutConstraint!
    
    // Dependencies
    var pm: SignInPM!
    
    // Private Properties
    private var newUser = NewUser()
    
    // Static Init
    static func create(pm: SignInPM) -> SignInUI {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let ui = storyboard.instantiateViewController(withIdentifier: "SignInUI") as! SignInUI
        ui.pm = pm
        
        return ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyTheme()
        setup()
    }
    
    private func applyTheme() {
        Theme.apply(self)
        Theme.apply(titleLabel: titleLabel)
        Theme.apply(redButton: signupButton)
    }
    
    private func setup() {
        titleLabel.text = pm.title
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        updateValidation()
    }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.height {
                self.signupButtonBottomConstraint.constant = keyboardHeight + 10
                self.view.layoutIfNeeded()
            }
        }, completion: nil)
    }
    
    @objc private func keyboardWillHide(_ notification: NSNotification) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.signupButtonBottomConstraint.constant = 20
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    // MARK - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return pm.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pm.sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return pm.rowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return pm.headerHeight
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return pm.sections[section].title
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item: SignInItemPM = pm.sections[indexPath.section].items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: pm.cellIdentifier, for: indexPath) as! SignInTableViewCell
        
        Theme.apply(cell)
        cell.clipsToBounds = false
        
        cell.iconImage.image = item.icon.withRenderingMode(.alwaysTemplate)
        cell.iconImage.tintColor = .inactiveColor
        cell.inputField.delegate = self
        cell.inputField.tag = item.type.rawValue
        cell.inputField.textContentType = item.contentType
        cell.inputField.keyboardType = item.keyboardType
        cell.inputField.isSecureTextEntry = item.secureTextEntry
        cell.inputField.placeholder = item.placeholder
        cell.inputField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        cell.detailLabel.text = item.detailLabel
        cell.separationView.backgroundColor = .separationLine
        
        return cell
    }
    
    // MARK - Text Field

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateEditingState(textField, active: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateEditingState(textField, active: false)
        updateValidation()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let type = SignInInputType(index: textField.tag),
            let input = textField.text else { return }
        
        switch type {
        case .firstName:
            newUser.firstName = input
        case .lastName:
            newUser.lastName = input
        case .email:
            newUser.email = input
        case .password:
            newUser.password = input
        case .newPassword:
            newUser.password = input
        }
        
        updateInputValidation(textField)
        updateValidation()
    }
    
    // MARK - Helper Methods
    
    private func updateValidation() {
//        nextButton.isEnabled =  true// newUser.isReadyToRegister
//        next2Button.isHidden =  false//!newUser.isReadyToRegister
    }
    
    func updateEditingState(_ textField: UITextField, active: Bool) {
//        let index = IndexPath(row: textField.tag, section: 0)
//        let row = pm.sections[0].items.
//        let cell = tableView.cellForRow(at: index) as! SignInTableViewCell
//        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
//            cell.separationView.backgroundColor = (active) ? .activeColor : .separationLine
//        }, completion: nil)
    }
    
    func updateInputValidation(_ textField: UITextField) {
//        guard let type = SignInInputType(index: textField.tag) else { return }
//        let index = IndexPath(row: textField.tag, section: 0)
//        let cell = tableView.cellForRow(at: index) as! SignInTableViewCell
//
//        var validation: Bool
//        switch type {
//        case .firstName:
//            validation = newUser.isValidFirstName
//        case .lastName:
//            validation = newUser.isValidLastName
//        case .email:
//            validation = newUser.isValidEmail
//        case .password:
//            validation = newUser.isValidPassword
//        case .newPassword:
//            validation = newUser.isValidPassword
//        }
//
//        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
//            cell.iconImage.tintColor = validation ? .activeColor : .inactiveColor
//        }, completion: nil)
    }
    
    private func loginUser(with email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            print("USER \(String(describing: user)) and Error \(String(describing: error))")
            guard let user = user else { return }
            print("USER \(user)")
            self.didFinish.onCompleted()
        }
    }
    
    private func registerUser(with email: String, password: String) {
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            
            guard let user = authResult?.user else { return }
            print("USER \(user)")
            self.didFinish.onCompleted()
        }
    }
    
    // MARK - Actions
    
    @IBAction func nextAction(_ sender: Any) {
        switch pm.type {
        case .login:
            loginUser(with: newUser.email, password: newUser.password)
        case .registration:
            registerUser(with: newUser.email, password: newUser.password)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        didFinish.onCompleted()
    }
    
}
