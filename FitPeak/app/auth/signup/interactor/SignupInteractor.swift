import Foundation
import RxSwift
import RxCocoa

protocol SignUpInteractorInputs {
    func bind(actions: SignUpUI.Actions)
}

protocol SignUpInteractorOutputs: InteractorOutputs {
    var submit: PublishSubject<UserSignup> { get }
    var back: PublishSubject<Void> { get }
}

protocol SignUpInteractorUIOutputs {
    var updateUI: Driver<SignUpPM> { get }
}

protocol SignUpType: Interactor {
    var inputs: SignUpInteractorInputs { get }
    var outputs: SignUpInteractorOutputs { get }
    var uiOutputs: SignUpInteractorUIOutputs { get }
}

typealias UserSignupSteam = Observable<UserSignup>

// MARK: - Interactor

final class SignUpInteractor: SignUpType, SignUpInteractorInputs, SignUpInteractorOutputs, SignUpInteractorUIOutputs {
    
    // MARK: - Inputs    
    func bind(actions: SignUpUI.Actions) {
        actions.submit.asObservable()
            .withLatestFrom(userSignup) { $1 }
            .bind(to: submit)
            .disposed(by: disposeBag)
        
        actions.updateInput.asObservable()
            .filterNil()
            .withLatestFrom(userSignup) { ($1, $0.0, $0.1) }
            .map({ (userSignup, input, type) in
                var newSignup = userSignup
                newSignup.update(input, of: type)
                return newSignup
            }).bind(to: userSignup)
            .disposed(by: disposeBag)

        actions.back.asObservable()
            .bind(to: back)
            .disposed(by: disposeBag)
    }
    
    // MARK: - UIOutputs
    lazy var updateUI: Driver<SignUpPM> = self.createUpdateUIStream()
    
    // MARK: - Scene Outputs
    var submit = PublishSubject<UserSignup>()
    var userSignup = BehaviorSubject<UserSignup>(value: UserSignup())
    var back = PublishSubject<Void>()
    var didFinish = PublishSubject<Void>()
    
    // MARK: - Input Output
    var inputs: SignUpInteractorInputs { return self }
    var outputs: SignUpInteractorOutputs { return self }
    var uiOutputs: SignUpInteractorUIOutputs { return self }
    
    // MARK: - Properties
    let disposeBag = DisposeBag()
    
    // MARK: - Private Properties
    private let presentationModelSubject = BehaviorRelay(value: SignUpPM())
    
    // MARK: - Lifecycle
    init() {
        bindUpdateStream()
    }
    
    // MARK: - UI Output Streams
    private func createUpdateUIStream() -> Driver<SignUpPM> {
        return presentationModelSubject.asDriver()
    }
    
    private func bindUpdateStream() {
        userSignup.asObservable()
            .map { SignUpPM($0.isReadyToSignUp) }
            .bind(to: presentationModelSubject)
            .disposed(by: disposeBag)
    }
    
}
