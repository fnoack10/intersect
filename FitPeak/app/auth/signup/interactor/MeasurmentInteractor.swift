import Foundation
import RxSwift
import RxCocoa

protocol MeasurementInteractorInputs {
    func bind(actions: MeasurementUI.Actions)
}

protocol MeasurementInteractorOutputs: InteractorOutputs {
    var signupCompleted: PublishSubject<Void> { get }
    var back: PublishSubject<Void> { get }
}

protocol MeasurementInteractorUIOutputs {
    var updateUI: Driver<MeasurementPM> { get }
}

protocol MeasurementType: Interactor {
    var inputs: MeasurementInteractorInputs { get }
    var outputs: MeasurementInteractorOutputs { get }
    var uiOutputs: MeasurementInteractorUIOutputs { get }
}

// MARK: - Interactor

final class MeasurementInteractor: MeasurementType, MeasurementInteractorInputs, MeasurementInteractorOutputs, MeasurementInteractorUIOutputs {
    
    // MARK: - Inputs
    func bind(actions: MeasurementUI.Actions) {
        actions.signup.asObservable()
            .flatMap { self.gateway.signupUser(with: self.userSignup) }
            .subscribe(onNext: { result in
                switch result {
                case .success:
                    self.signupCompleted.onNext(())
                case let .failure(error):
                    print("Error \(error)")
                    // Handle Error
                    break
                }
            }).disposed(by: disposeBag)
        
        actions.selectedMeasurment.asObservable()
            .do(onNext: { self.userSignup.unitSystem = $0 })
            .map { MeasurementPM(selectedMeasurment: $0) }
            .bind(to: presentationModelSubject)
            .disposed(by: disposeBag)
        
        actions.back.asObservable()
            .bind(to: back)
            .disposed(by: disposeBag)
    }
    
    // MARK: - UIOutputs
    lazy var updateUI: Driver<MeasurementPM> = self.createUpdateUIStream()
    
    // MARK: - Scene Outputs
    var signupCompleted = PublishSubject<Void>()
    var back = PublishSubject<Void>()
    var didFinish = PublishSubject<Void>()
    
    // MARK: - Input Output
    var inputs: MeasurementInteractorInputs { return self }
    var outputs: MeasurementInteractorOutputs { return self }
    var uiOutputs: MeasurementInteractorUIOutputs { return self }
    
    // MARK: - Properties
    let disposeBag = DisposeBag()
    
    // MARK: - Private Properties
    private let gateway: AuthGate
    private var presentationModelSubject = BehaviorRelay(value: MeasurementPM())
    private var userSignup: UserSignup
    
    // MARK: - Lifecycle
    init(gateway: AuthGate, userSignup: UserSignup) {
        self.gateway = gateway
        self.userSignup = userSignup
    }
    
    // MARK: - UI Output Streams
    private func createUpdateUIStream() -> Driver<MeasurementPM> {
        return presentationModelSubject.asDriver()
    }
    
}
