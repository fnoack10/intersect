import Foundation
import Firebase

enum UnitSystem: String {
    case metric
    case english
    
    var title: String {
        switch self {
        case .metric: return "Metric [kg]"
        case .english: return "English [lb]"
        }
    }
    
    var units: String {
        switch self {
        case .metric: return "kg"
        case .english: return "lb"
        }
    }
    
    static func userUnits() -> String {
        guard let weightSystem = UserDefaults.standard.string(forKey: "unitSystem"),
            let unitSystem = UnitSystem.init(rawValue: weightSystem) else { return UnitSystem.metric.units }
        return unitSystem.units
    }
    
}
