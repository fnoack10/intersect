import Foundation
import UIKit
import FirebaseFirestore

struct Log {
    var id: String
    var dateOfScore: Date
    var formattedDate: String
    var score: Int
    var note: String
    var type: InputFieldType
    
    init(id: String,
         dateOfScore: Date,
         score: Int,
         note: String,
         movement: Movement) {
        
        self.id = id
        self.dateOfScore = dateOfScore
        self.formattedDate = dateOfScore.description
        self.score = score
        self.note = note
        self.type = movement.type
    }
    
    static func create(with id: String, data: [String: Any], movement: Movement) -> Log? {
        guard let timestamp = data["creationDate"] as? Timestamp else { return nil }
        guard let score = data["score"] as? Int else { return nil }
        guard let note = data["note"] as? String else { return nil }
        let dateOfScore = timestamp.dateValue()
        return Log.init(id: id, dateOfScore: dateOfScore, score: score, note: note, movement: movement)
    }
    
    var formattedScore: String {
        switch type {
        case .weight: return "\(score) \(UnitSystem.userUnits())"
        case .time: return TimeInterval(score).timeFromSeconds
        case .reps: return "\(score) reps"
        default: return "Unknown"
        }
    }
    
}
