import Foundation

struct Profile {
    var email: String
    var firstName: String
    var lastName: String
    var unitSystem: UnitSystem

    init(email: String,
         firstName: String,
         lastName: String,
         unitSystem: UnitSystem) {
        
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.unitSystem = unitSystem
    }
    
    static func create(with data: [String: Any]) -> Profile? {
        guard let email = data["email"] as? String else { return nil }
        guard let firstName = data["firstName"] as? String else { return nil }
        guard let lastName = data["lastName"] as? String else { return nil }
        guard let metric = data["unitSystem"] as? String else { return nil }
        guard let unitSystem = UnitSystem.init(rawValue: metric) else { return nil }
        return Profile(email: email, firstName: firstName, lastName: lastName, unitSystem: unitSystem)
    }

}
