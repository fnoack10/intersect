import Foundation
import UIKit

struct Discipline {
    var id: String
    var label: String
    var movements: [Movement]
    
    init(id: String,
         label: String,
         movements: [Movement]) { 
        
        self.id = id
        self.label = label
        self.movements = movements
    }
    
    static func create(with id: String, data: [String: Any], movements: [Movement]) -> Discipline? {
        guard let label = data["label"] as? String else { return nil }
        return Discipline(id: id, label: label, movements: movements)
    }
}
