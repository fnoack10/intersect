import Foundation
import UIKit
import RxSwift

class Movement: Equatable {
    let disposeBag = DisposeBag()
    var updateStream: Observable<[Log]>?
    
    var id: String = ""
    var disciplineId: String = ""
    var label: String = ""
    var type: InputFieldType = .weight
    var logs: [Log] = []
    
    lazy var needsUpdate: Observable<Void> = self.createNeedsUpdateStream()
    
    init() {}
    
    init(id: String, disciplineId: String , label: String, type: InputFieldType) {
        self.id = id
        self.disciplineId = disciplineId
        self.label = label
        self.type = type
    }
    
    static func create(with id: String, disciplineId: String, data: [String: Any]) -> Movement? {
        guard let label = data["label"] as? String else { return nil }
        guard let inputType = data["type"] as? String else { return nil }
        guard let type = InputFieldType.init(rawValue: inputType) else { return nil }
        return Movement(id: id, disciplineId: disciplineId, label: label, type: type)
    }
    
    static func ==(lhs: Movement, rhs: Movement) -> Bool {
        return lhs.id == rhs.id
    }
    
    private func createNeedsUpdateStream() -> Observable<Void> {
        guard let updateStream = self.updateStream else { return Observable.just(()) }
        return updateStream.map { _ in () }
    }
    
    func updateLogsWithStream(_ stream: Observable<[Log]>) {
        self.updateStream = stream
        stream.subscribe(onNext: { logs in
            self.logs = logs
        }).disposed(by: disposeBag)
    }
    
    var isHidden: Bool { logs.isEmpty }
    var bestScore: Int {
        switch type.direction {
        case .max: return logs.map { $0.score }.max() ?? 0
        case .min: return logs.map { $0.score }.min() ?? 0
        }
    }
    
    var formattedBestScore: String { self.formattedScore(score: bestScore) }
    var mostRecentLog: Date? { logs.map { $0.dateOfScore }.max() }
    
    var formattedMostRecentLog: String {
        guard let date =  mostRecentLog else { return "Add first entry" }
        return date.timeAgo()
    }
    
    func formattedScore(score: Int) -> String {
        switch type {
        case .weight: return "\(score) \(UnitSystem.userUnits())"
        case .time: return TimeInterval(score).timeFromSeconds
        case .reps: return "\(score) reps"
        default: return "Unknown"
        }
    }
    
    var bestScoreTimeComponents: (Int, Int, Int) {
        let score = bestScore
        let hour = score / 3600
        let minute = score / 60 % 60
        let second = score % 60
        return (hour, minute, second)
    }
    
    func userUnits() -> String {
        guard let weightSystem = UserDefaults.standard.string(forKey: "unitSystem"),
            let unitSystem = UnitSystem.init(rawValue: weightSystem) else { return UnitSystem.metric.units }
        return unitSystem.units
    }

}


