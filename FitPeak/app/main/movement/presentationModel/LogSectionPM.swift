import Foundation

struct LogSectionPM {
    
    var title: String
    var items: [LogItemPM]
    
    init(items: [LogItemPM]) {
        self.title = "History"
        self.items = items
    }
    
}
