import Foundation
import UIKit

struct LogItemPM {
    let movement: Movement
    let score: Int
    let creationDate: Date
    let note: String
    
    init(movement: Movement, creationDate: Date, score: Int, note: String) {
        self.movement = movement
        self.creationDate = creationDate
        self.score = score
        self.note = note
    }
    
    var title: String { return movement.formattedScore(score: score) }
    var standardDate: String { return creationDate.standardDate }
    var isBest: Bool {
        let scores = movement.logs.map { $0.score }
        switch movement.type.direction {
        case .max:
            guard let max = scores.max() else { return false }
            return score == max
        case .min:
            guard let min = scores.min() else { return false }
            return score == min
        }
    }
    
    func attributedDescription(from items: [LogItemPM], index: Int) -> NSAttributedString {
        var sign: String = ""
        var growthPercentage: String = ""
        let previusIndex = index+1
        if previusIndex < items.count {
            let previousItem = items[previusIndex]
            let growth = score-previousItem.score
            
            if growth > 0 { sign = "+" }
            growthPercentage = sign + movement.formattedScore(score: growth)
            if growth == 0 { growthPercentage = "" }
        }
        
        var growthColor: UIColor = .detailTextColor
        switch movement.type.direction {
        case .max:
            growthColor = sign != "" ? UIColor.activeTextColor : UIColor.red
        case .min:
            growthColor = sign != "" ? UIColor.red : UIColor.activeTextColor
        }
        
        let bullet = growthPercentage == "" || note == "" ? "" : " • "
        let attributedString = NSMutableAttributedString(string: growthPercentage,
                                                         attributes: [.font: UIFont(.semibold, 12),
                                                                      .foregroundColor: growthColor])
        let attributedNotes = NSMutableAttributedString(string: bullet + note,
                                                        attributes: [.font: UIFont(.semibold, 12),
                                                                     .foregroundColor: UIColor.detailTextColor])
        attributedString.append(attributedNotes)
        return attributedString
        
    }
    
}
