import Foundation
import UIKit
import FirebaseFirestore

struct MovementOverviewPM {
    var cellIdentifier: String = "LogTableViewCell"
    var headerHeight: CGFloat = 50.0
    var rowHeight: CGFloat = 65

    var title: String
    let movement: Movement
    var graphPoints: [Double] = []
    var history: LogSectionPM = LogSectionPM(items: [])
    
    init(movement: Movement) {
        self.title = movement.label
        self.movement = movement
        let items = movement.logs.map {
            LogItemPM(movement: self.movement, creationDate: $0.dateOfScore, score: $0.score, note: $0.note)
        }
        
        history = LogSectionPM(items: items)
        prepareGraphPoints()
    }
    
    var recordTitle: String { return movement.formattedBestScore }
    
    func recordSpacing(graphView: UIView, recordView: UIView) -> CGFloat {
        var recordIndex: Int = 0
        let recordViewHalfWidth = recordView.frame.width/2
        
        // Check if all values are the same -> Show in the middle
        if graphPoints.dropLast().allSatisfy({ $0 == graphPoints.last }) {
            return graphView.frame.width/2 - recordViewHalfWidth
        }
        
        switch movement.type.direction {
        case .max:
            guard let max = graphPoints.max(),
                let index = graphPoints.index(of: max) else { return 0 }
            recordIndex = index
        case .min:
            guard let min = graphPoints.min(),
                let index = graphPoints.index(of: min) else { return 0 }
            recordIndex = index
        }
        let spacing = graphView.frame.width/CGFloat(integerLiteral:graphPoints.count-1)
        return spacing*CGFloat(recordIndex)-recordViewHalfWidth
    }
        
    mutating func prepareGraphPoints() {
        let items = history.items
        if items.count > 0 {
            var points: [Double] = items.map { Double($0.score) }
            switch movement.type.direction {
            case .max:
                points.append(0)
                points.append(0)
                points.insert(points[0], at: 0)
            case .min:
                points.insert(points[0], at: 0)
                if let lastPoint = points.last {
                    points.append(lastPoint)
                }
            }
            graphPoints = points.reversed()
        }
    }
}
