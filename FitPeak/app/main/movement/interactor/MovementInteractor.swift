import Foundation
import RxSwift
import RxCocoa

protocol MovementInteractorInputs {
    func bind(actions: MovementOverviewUI.Actions)
}

protocol MovementInteractorOutputs: InteractorOutputs {
    var back: PublishSubject<Void> { get }
    var add: PublishSubject<Void> { get }
}

protocol MovementInteractorUIOutputs {
    var updateUI: Driver<MovementOverviewPM> { get }
}

protocol MovementProtocolType: Interactor {
    var inputs: MovementInteractorInputs { get }
    var outputs: MovementInteractorOutputs { get }
    var uiOutputs: MovementInteractorUIOutputs { get }
}

// MARK: - Interactor

final class MovementInteractor: MovementProtocolType, MovementInteractorInputs, MovementInteractorOutputs, MovementInteractorUIOutputs {
    
    // MARK: - Inputs
    func bind(actions: MovementOverviewUI.Actions) {
        actions.back.asObservable()
            .bind(to: back)
            .disposed(by: disposeBag)
        
        actions.add.asObservable()
            .bind(to: add)
            .disposed(by: disposeBag)
        
        actions.delete.asObservable()
            .filterNil()
            .map { self.movement.logs[$0] }
            .flatMap { self.gateway.deleteLog(with: $0, movementID: self.movement.id) }
            .subscribe(onNext: { result in
                print("RESULT \(result)")
                switch result {
                case .success:
                    print("success")
                case .failure:
                    print("failure")
                }
            }).disposed(by: disposeBag)
        
    
    }

    // MARK: - UIOutputs
    lazy var updateUI: Driver<MovementOverviewPM> = self.createUpdateUIStream()

    // MARK: - Scene Outputs
    var back: PublishSubject<Void> = PublishSubject<Void>()
    var add: PublishSubject<Void> = PublishSubject<Void>()
    var didFinish = PublishSubject<Void>()

    // MARK: - Input Output
    var inputs: MovementInteractorInputs { return self }
    var outputs: MovementInteractorOutputs { return self }
    var uiOutputs: MovementInteractorUIOutputs { return self }

    // MARK: - Properties
    let disposeBag = DisposeBag()

    // MARK: - Private Properties
    private let gateway: AppGate
    private let movement: Movement
    private let presentationModelSubject: BehaviorRelay<MovementOverviewPM>
    
    // MARK: - Lifecycle
    init(gateway: AppGate, movement: Movement) {
        self.gateway = gateway
        self.movement = movement
        self.presentationModelSubject = BehaviorRelay(value: MovementOverviewPM(movement: movement))
        
        bindMovement()
    }
    
    private func bindMovement() {
        movement.needsUpdate
            .map { MovementOverviewPM(movement: self.movement) }
            .bind(to: presentationModelSubject)
            .disposed(by: disposeBag)
    }
    
    // MARK: - UI Output Streams
    
    private func createUpdateUIStream() -> Driver<MovementOverviewPM> {
        return presentationModelSubject.asDriver()
    }
    
}
