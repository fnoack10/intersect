import UIKit
import RxSwift
import RxCocoa
import ScrollableGraphView

class MovementOverviewUI: InteractableUI<MovementInteractor>, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    var didFinish = PublishSubject<Void>()
    var didTapNew = PublishSubject<Void>()
    var didTapDelete = PublishSubject<Int?>()
    var disposedBag = DisposeBag()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var recordView: UIView!
    @IBOutlet weak var recordLabel: UILabel!
    @IBOutlet weak var recordTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var recordLeadingConstraint: NSLayoutConstraint!
    
    private lazy var backButtonItem: UIBarButtonItem = self.createBarButtonItem(type: .back)
    private lazy var addButtonItem: UIBarButtonItem = self.createBarButtonItem(type: .add)
    
    private var presentationModel = MovementOverviewPM(movement: Movement())
    private var graph: ScrollableGraphView?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        applyTheme()
        
        setupUI()
        setupGraph()
    }
    
    // MARK: - Theme
    private func applyTheme() {
        MovementOverviewTheme.apply(self)
        MovementOverviewTheme.apply(recordView: recordView)
        MovementOverviewTheme.apply(recordLabel: recordLabel)
        MovementOverviewTheme.apply(separatorView: separatorView)
    }

    // MARK: - Setup
    private func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = presentationModel.rowHeight
        tableView.rowHeight = UITableView.automaticDimension
        
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationItem.leftBarButtonItem = backButtonItem
        navigationItem.rightBarButtonItem = addButtonItem
    }
    
    private func createBarButtonItem(type: NavigationItemType) -> UIBarButtonItem {
        let barButtonItem = UIBarButtonItem(image: type.icon, style: .plain, target: self, action: nil)
        barButtonItem.tintColor = .navigationTintDark
        return barButtonItem
    }
    
    override func bindInteractor(interactor: MovementInteractor) {
        interactor.uiOutputs.updateUI.drive(onNext: { [weak self] presentationModel in
            self?.presentationModel = presentationModel
            self?.updateUI(presentationModel)
        }).disposed(by: disposedBag)
    }
    
    private func updateUI(_ presentationModel: MovementOverviewPM) {
        title = presentationModel.title
        tableView.reloadData()
        updateGraph()
    }
    
    private func setupGraph() {
        resetRecordView()
    }
    
    // MARk: - UI
    private func updateUI() {
        tableView.reloadData()
        updateGraph()
    }
    
    func updateGraph() {
        if let graph = self.graph {
            graph.removeFromSuperview()
            self.graph = nil
        }
        resetRecordView()
        
        if presentationModel.graphPoints.count > 0 {
            addGraph()
            updateRecordView()
        }
    }
    
    func resetRecordView() {
        recordTopConstraint.constant = graphView.frame.height
        self.view.layoutIfNeeded()
    }
    
    func updateRecordView() {
        recordLeadingConstraint.constant = presentationModel.recordSpacing(graphView: graphView, recordView: recordView)
        recordLabel.text = presentationModel.recordTitle
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.8, delay: 0.5, options: .curveEaseInOut, animations: {
            self.recordTopConstraint.constant = 20
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentationModel.history.items.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return presentationModel.headerHeight
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return TableViewHeader.createPrimary(title: presentationModel.history.title, height: presentationModel.headerHeight, view: view)
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { [weak self] (action, view, completion) in
            self?.didTapDelete.onNext(indexPath.row)
            completion(true)
        }
        delete.image = Asset.Movement.delete.image
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item: LogItemPM = presentationModel.history.items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: presentationModel.cellIdentifier, for: indexPath) as! LogTableViewCell
        
        cell.dateLabel.text = item.standardDate
        cell.titleLabel.text = item.title
        cell.bellImageView.isHidden = !item.isBest
        cell.descriptionLabel.attributedText = item.attributedDescription(from: presentationModel.history.items, index: indexPath.row)
        
        cell.topConnectionView.isHidden = indexPath.row == 0
        if indexPath.row == presentationModel.history.items.count-1 {
            cell.separatorInset = .zero
        } else {
            cell.separatorInset = UIEdgeInsets(top: 0, left: 100, bottom: 0, right: 0)
        }
        
        return cell
    }
    
    // MARK: - Action
    @objc private func plusAction() {
        didTapNew.onNext(())
    }
    
    @objc private func backAction() {
        didFinish.onCompleted()
    }
}

// MARK: - Graph
extension MovementOverviewUI: ScrollableGraphViewDataSource {
    
    func addGraph() {
        if self.graph != nil { return }
        
        // Check if all values are 0 - Causes a Assert failure in Graph Framework
        if presentationModel.graphPoints.dropLast().allSatisfy({ $0 == 0 }) { return }
        
        let graph = ScrollableGraphView(frame: graphView.bounds, dataSource: self)
        graph.isUserInteractionEnabled = false
        graph.topMargin = 40
        graph.bottomMargin = 30
        graph.leftmostPointPadding = 0
        graph.rightmostPointPadding = 0
        graph.backgroundFillColor = .clear
        graph.rangeMax = presentationModel.graphPoints.max()!*1.10
        graph.dataPointSpacing = (graphView.frame.width)/CGFloat(integerLiteral: presentationModel.graphPoints.count-1)
        graph.isScrollEnabled = false
        
        let linePlot = LinePlot(identifier: "consumption")
        linePlot.adaptAnimationType = .elastic
        linePlot.lineColor = .lineGray
        linePlot.lineStyle = .smooth
        linePlot.lineWidth = 4.0
        linePlot.shouldFill = true
        linePlot.fillType = .solid
        linePlot.fillColor = .backgroundColorSecondary
        
        graph.addPlot(plot: linePlot)
        
        self.graphView.addSubview(graph)
        self.graph = graph
    }
    
    func value(forPlot plot: Plot, atIndex pointIndex: Int) -> Double {
        return presentationModel.graphPoints[pointIndex]
    }
    
    func label(atIndex pointIndex: Int) -> String {
        return ""
    }
    
    func numberOfPoints() -> Int {
        return presentationModel.graphPoints.count
    }
    
}

// MARK: UI Actions

extension MovementOverviewUI {
    
    struct Actions {
        let back: Driver<Void>
        let add: Driver<Void>
        let delete: Driver<Int?>
    }
    
    var actions: Actions {
        return Actions(
            back: backButtonItem.rx.tap.asDriver(onErrorJustReturn: ()),
            add: addButtonItem.rx.tap.asDriver(onErrorJustReturn: ()),
            delete: didTapDelete.asDriver(onErrorJustReturn: nil))
    }
}
