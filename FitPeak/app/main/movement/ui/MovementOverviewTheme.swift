import UIKit

struct MovementOverviewTheme: ThemeProtocol {
    
    static func apply(separatorView: UIView) {
        separatorView.backgroundColor = .lineGray
    }
    
    static func apply(recordView: UIView) {
        recordView.backgroundColor = .activeTextColor
        recordView.layer.cornerRadius = 8.0
    }
    
    static func apply(recordLabel: UILabel) {
        recordLabel.textColor = .lightTextColor
        recordLabel.font = UIFont(.bold, 12)
    }
    
}


