import Foundation
import RxSwift

final class TabScene: Scene {
    
    // MARK: - Properties
    var didFinish = PublishSubject<Void>()
    var children: [Scene] = []
    var disposeBag = DisposeBag()
    
    // MARK: - Private Properties
    private let gateway: AppGate
    private let tabContext: TabContext

    // MARK: - Lifecycle
    init(gateway: AppGate, tabContext: TabContext) {
        self.gateway = gateway
        self.tabContext = tabContext
    }
    
    func start() {
        launchMainScene()
        launchProfileScene()
        tabContext.present()
        Appearance.tabBar()
    }
}

// MARK: - UI
extension TabScene {
    
    private func launchMainScene() {
        let navigationContext = NavigationControllerContext(parent: tabContext)
        let mainScene = MainScene(gateway: gateway, navigationContext: navigationContext)
        mainScene.start()
        addChild(mainScene)
    }
    
    private func launchProfileScene() {
        let navigationContext = NavigationControllerContext(parent: tabContext)
        let profileScene = ProfileScene(gateway: gateway, navigationContext: navigationContext)
        profileScene.start()
        addChild(profileScene)
        
        profileScene.didFinish
            .subscribe(onCompleted: { [weak self] in
                self?.didFinish.onCompleted()
        }).disposed(by: disposeBag)
    }
    
}
