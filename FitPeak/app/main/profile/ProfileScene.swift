import Foundation
import RxSwift
import Firebase

final class ProfileScene: Scene {
    
    // MARK: - Properties
    var didFinish = PublishSubject<Void>()
    var children: [Scene] = []
    var disposeBag = DisposeBag()
    
    // MARK: - Private Properties
    private let gateway: AppGate
    private var navigationContext: NavigationControllerContext
    
    // MARK: - Lifecycle
    init(gateway: AppGate, navigationContext: NavigationControllerContext) {
        self.gateway = gateway
        self.navigationContext = navigationContext
    }
    
    func start() {
        launchProfileUI()
        setupTabBar()
    }
    
    private func setupTabBar() {
        let tabBarItem = UITabBarItem(
            title: "Profile",
            image: Asset.TabBar.profile.image,
            selectedImage: Asset.TabBar.profile.image)
        tabBarItem.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        navigationContext.navigationController.tabBarItem = tabBarItem
    }
    
}

extension ProfileScene {
    
    private func launchProfileUI() {
        
        let ui = ProfileUI.create(scene: StoryboardScene.Profile.profileUI) { profileUI in
            // This should produce exactly one type of UI
            guard let profileUI = profileUI as? ProfileUI else { fatalError() }
            
            // Interactor
            let interactor = ProfileInteractor(gateway: self.gateway)
            interactor.inputs.bind(actions: profileUI.actions)
            interactor.inputs.configure(with: ProfilePM())
            
            interactor.outputs.personalInformation
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { [weak self] _ in
                    self?.launchPersonalInformationUI()
            }).disposed(by: profileUI.disposeBag)
            
            interactor.outputs.feedback
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { [weak self] type in
                    self?.launchFeedbackUI(type: type)
                }).disposed(by: profileUI.disposeBag)
            
            interactor.didFinish
                .subscribe(onCompleted: { [weak self] in
                    self?.didFinish.onCompleted()
                }).disposed(by: profileUI.disposeBag)
            
            
            return interactor
            
        }
        navigationContext.push(ui, animated: false, mode: .topmost, completion: nil)
    }
    
    private func launchPersonalInformationUI() {
        guard let user = Auth.auth().currentUser else { return }
        let presentationModel = PersonalInformationPM(user: user)
        let personalInformationUI = PersonalInformationUI.create(presentationModel: presentationModel)
        
        personalInformationUI.didFinish
            .subscribe(onCompleted: { [weak self] in
                self?.navigationContext.pop(animated: true)
        }).disposed(by: disposeBag)
    
        navigationContext.push(personalInformationUI)
    }
    
    private func launchFeedbackUI(type: FeedbackType) {
        let presentationModel = FeedbackPM(type: type)
        let feedbackUI = FeedbackUI.create(presentationModel: presentationModel)
        
        feedbackUI.didFinish
            .subscribe(onCompleted: { [weak self] in
                self?.navigationContext.pop(animated: true)
            }).disposed(by: disposeBag)
        
        navigationContext.push(feedbackUI)
        
    }
}
