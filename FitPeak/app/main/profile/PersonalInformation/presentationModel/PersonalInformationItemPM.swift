import Foundation

struct PersonalInformationItemPM {
    let type: UserInputType
    let title: String
    
    init(type: UserInputType) {
        self.type = type
        switch type {
        case .firstName:
            title = "First Name"
        case .lastName:
            title = "Last Name"
        case .email:
            title = "Email"
        default:
            title = "Unkown"
        }
        
    }
    
}
