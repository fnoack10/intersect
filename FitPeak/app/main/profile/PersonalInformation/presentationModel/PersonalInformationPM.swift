import Foundation
import UIKit
import Firebase

struct PersonalInformationPM {
    
    var title: String
    var cellIdentifier: String = "PersonalInformationTableViewCell"
    var sections: [PersonalInformationSectionPM] = []
    var headerHeight: CGFloat = 30.0
    var rowHeight: CGFloat = 55.0
    
    init(user: User?) {
        title = "Personal Information"
        guard let user = user else { return }
        sections = [PersonalInformationSectionPM(type: .firstName, user: user),
                    PersonalInformationSectionPM(type: .email, user: user)]
        
    }
    
    
}
