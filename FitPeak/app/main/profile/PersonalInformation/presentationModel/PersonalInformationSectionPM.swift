import Foundation
import Firebase

struct PersonalInformationSectionPM {
    var type: UserInputType
    var title: String
    var value: String
    var numberOfRowsInSection: Int = 1
    
    init(type: UserInputType, user: User) {
        self.type = type
        switch type {
        case .firstName:
            title = "Name"
            value = user.displayName ?? ""
        case .email:
            title = "Email"
            value = user.email ?? ""
        default:
            title = "Unkown"
            value = "Unkown"
        }
    }
    
}
