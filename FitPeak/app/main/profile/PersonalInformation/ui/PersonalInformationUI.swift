import UIKit
import RxSwift
import Firebase

class PersonalInformationUI: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var didFinish = PublishSubject<Void>()
    var disposedBag = DisposeBag()
    
    @IBOutlet weak var tableView: UITableView!
    
    private var pm = PersonalInformationPM(user: nil)
    
    // Static Init
    static func create(presentationModel: PersonalInformationPM) -> PersonalInformationUI {
        let storyboard: UIStoryboard = UIStoryboard(name: "PersonalInformation", bundle: nil)
        let ui = storyboard.instantiateViewController(withIdentifier: "PersonalInformationUI") as! PersonalInformationUI
        ui.pm = presentationModel
        return ui
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyTheme()
        setupUI()
        setupNavigationUI()
    }
    
    private func applyTheme() {
        PersonalInformationTheme.apply(self)
    }
    
    private func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
    }
    
    private func setupNavigationUI() {
        title = pm.title
        
        let backItem: NavigationItemType = .back
        let backButtonItem = UIBarButtonItem(image: backItem.icon, style: .plain, target: self, action: #selector(backAction))
        backButtonItem.tintColor = .navigationTintDark
        navigationItem.leftBarButtonItem = backButtonItem
    }
    
    // MARK - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return pm.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pm.sections[section].numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return pm.rowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return pm.headerHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionItem = pm.sections[section]
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: pm.headerHeight))
        headerView.backgroundColor = .white
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 0, width: view.frame.width, height: headerView.frame.height))
        headerLabel.font = UIFont(.bold, 12)
        headerLabel.textColor = .primaryTextColor
        headerLabel.text = sectionItem.title.uppercased()
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section: PersonalInformationSectionPM = pm.sections[indexPath.section]
        let cell = tableView.dequeueReusableCell(withIdentifier: pm.cellIdentifier, for: indexPath) as! PersonalInformationTableViewCell
        
        cell.titleLabel.text = section.value
        cell.lineView.backgroundColor = .lineGray
        
        return cell
    }
    
    @objc private func backAction() {
        didFinish.onCompleted()
    }
}
