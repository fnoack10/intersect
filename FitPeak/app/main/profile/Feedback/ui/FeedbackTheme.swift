import UIKit

struct FeedbackTheme: ThemeProtocol {
    
    static func apply(_ countLabel: UILabel, isValid: Bool) {
        countLabel.textColor = isValid ? .primaryTextColor : .errorTextColor
    }

}
