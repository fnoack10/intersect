import UIKit
import RxSwift
import FirebaseAuth
import FirebaseFirestore

class FeedbackUI: UIViewController {
    
    var didFinish = PublishSubject<Void>()
    var disposeBag = DisposeBag()
    
    // MARK: - Outlets
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Private Properties
    private var db: Firestore!
    private var pm = FeedbackPM(type: .feedback)
    
    // Static Init
    static func create(presentationModel: FeedbackPM) -> FeedbackUI {
        let storyboard: UIStoryboard = UIStoryboard(name: "Feedback", bundle: nil)
        let ui = storyboard.instantiateViewController(withIdentifier: "FeedbackUI") as! FeedbackUI
        ui.pm = presentationModel
        return ui
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        applyTheme()
        
        setupUI()
        setupNavigationUI()
        setupDatabase()
        
        bindTextView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterKeyboardNotifications()
    }
    
    fileprivate func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    fileprivate func deregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo!
        let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            self.bottomConstraint.constant = keyboardHeight-10
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseInOut, animations: {
            self.bottomConstraint.constant = 100
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // MARK: - Setup
    private func applyTheme() {
        FeedbackTheme.apply(self)
        FeedbackTheme.apply(descriptionLabel)
        FeedbackTheme.apply(textView)
        FeedbackTheme.apply(countLabel)
        FeedbackTheme.apply(sendButton)
    }
    
    private func setupUI() {
        textView.text = ""
        textView.delegate = self
        descriptionLabel.text = pm.description
        countLabel.text = pm.countLimit.description
        sendButton.setTitle(pm.action, for: .normal)
        activityIndicator.hidesWhenStopped = true
    }
    
    private func setupNavigationUI() {
        title = pm.title
        
        let backButtonItem = UIBarButtonItem(image: NavigationItemType.back.icon, style: .plain, target: self, action: #selector(backAction))
        backButtonItem.tintColor = .navigationTintDark
        navigationItem.leftBarButtonItem = backButtonItem
    }
    
    private func setupDatabase() {
        let settings = FirestoreSettings()
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
    }
    
    private func bindTextView() {
        textView.rx.text
            .filterNil()
            .map { self.characters(from: $0) }
            .map { !($0.isEmpty || self.pm.countLimit < $0.count) }
            .bind(to: sendButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }
    
    // MARK: - Helpers
    private func characters(from string: String) -> String {
        return string.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    private func clearFeedback() {
        self.textView.text = ""
        validateInput()
    }

    private func validateInput() {
        let currentCount = pm.countLimit - characters(from: textView.text).count
        countLabel.text = currentCount.description

        let validCount = currentCount >= 0
        FeedbackTheme.apply(countLabel, isValid: validCount)
    }

    // MARK: - Action
    @objc private func backAction() {
        didFinish.onCompleted()
    }
    
    @IBAction func sendAction(_ sender: Any) {
        startLoading()
        let reportKey = pm.type.path
        db.collection("\(reportKey)").addDocument(data: pm.export()) { error in
            self.endLoading()
            if error != nil {
                print("An error happened!")
            } else {
                print("Data has been saved")
            }
            self.didFinish.onCompleted()
        }
    } 
    
    // MARK - Activity
    
    private func startLoading() {
        activityIndicator.startAnimating()
        sendButton.isEnabled = false
    }
    
    private func endLoading() {
        activityIndicator.stopAnimating()
        sendButton.isEnabled = true
    }

}

extension FeedbackUI: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        validateInput()
        pm.feedback = textView.text
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
}
