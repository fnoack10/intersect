import Foundation
import UIKit
import Firebase

enum FeedbackType {
    case feedback
    case bugReport
    
    var path: String {
        switch self {
        case .feedback: return "feedback"
        case .bugReport: return "bugReport"
        }
    }
    
    var title: String {
        switch self {
        case .feedback: return "Feedback"
        case .bugReport: return "Bug Report"
        }
    }
    
    var description: String {
        switch self {
        case .feedback: return "We really appreciate you taking the time to give us your feedback."
        case .bugReport: return "Thank you for taking the time to report issues, we will try to solve them as soon as possible."
        }
    }
    
    var action: String {
        switch self {
        case .feedback: return "Send Feedback"
        case .bugReport: return "Send Bug Report"
        }
    }
}

struct FeedbackPM {
    var type: FeedbackType
    var title: String
    var description: String
    var countLimit: Int = 250
    var action: String
    var feedback: String = ""
    
    init(type: FeedbackType) {
        self.type = type
        self.title = type.title
        self.description = type.description
        self.action = type.action
    }
    
    func export() -> [String: Any] {
        guard let user = Auth.auth().currentUser,
            let name = user.displayName,
            let email = user.email else { return ["creationDate": Date(), "feedback": feedback] }
        return ["creationDate": Date(),
                "feedback": feedback,
                "userID": user.uid,
                "name": name,
                "email": email]
    }
}
