import Foundation
import UIKit

struct ProfilePM {
    var title: String
    var cellIdentifier: String = "ProfileTableViewCell"
    var sections: [ProfileSectionPM] = [ProfileSectionPM(type: .account), ProfileSectionPM(type: .about)]
    var rowHeight: CGFloat = 55.0
    var headerHeight: CGFloat = 40.0
    var footerHeight: CGFloat = 140.0
    
    init(profile: Profile) {
        title = "Hello, \(profile.firstName)."
    }
    
    init() {
        title = "Profile"
    }

}
