//
//  ProfileItemPM.swift
//  Intersect
//
//  Created by Franco Noack on 21.12.18.
//  Copyright © 2018 Intersect. All rights reserved.
//

import UIKit

enum ProfileItemType {
    case personalInformation
    case logout
    case reportBug
    case suggestImprovements
    case rateApp
    
    var title: String {
        switch self {
        case .personalInformation: return "Personal Information"
        case .logout: return "Logout"
        case .reportBug: return "Report a Bug"
        case .suggestImprovements: return "Suggest Improvements"
        case .rateApp: return "Rate this App"
        }
    }
    
    var icon: UIImage {
        switch self {
        case .personalInformation: return UIImage(named: "personalInformation") ?? UIImage()
        case .logout: return UIImage(named: "logout") ?? UIImage()
        case .reportBug: return UIImage(named: "bug") ?? UIImage()
        case .suggestImprovements: return UIImage(named: "improvements") ?? UIImage()
        case .rateApp: return UIImage(named: "improvements") ?? UIImage()
        }
    }
    
    var accessoryType: UITableViewCell.AccessoryType {
        switch self {
        case .logout: return .none
        default: return .disclosureIndicator
        }
    }
    
}

struct ProfileItemPM {
    var type: ProfileItemType
    var title: String
    var icon: UIImage
    var accessoryType: UITableViewCell.AccessoryType = .none
    
    init(type: ProfileItemType) {
        self.type = type
        self.icon = type.icon
        self.title = type.title
        self.accessoryType = type.accessoryType
    }
    
}
