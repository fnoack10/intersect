import Foundation

struct ProfileSectionPM {
    
    var title: String
    var items: [ProfileItemPM]
    var hasFooter: Bool = false
    
    enum ProfileSectionType {
        case account
        case about
    }

    init(type: ProfileSectionType) {
        switch type {
        case .account:
            title = "Account"
            items = [ProfileItemPM(type: .personalInformation),
                     ProfileItemPM(type: .logout)]
        case .about:
            title = "Feedback"
            items = [ProfileItemPM(type: .reportBug),
                     ProfileItemPM(type: .suggestImprovements)]
                     //ProfileItemPM(type: .rateApp)]
            hasFooter = true
        }
    }
    
}
