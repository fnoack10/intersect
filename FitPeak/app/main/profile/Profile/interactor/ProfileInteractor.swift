import Foundation
import RxSwift
import RxCocoa

protocol ProfileInteractorInputs {
    func configure(with presentationModel: ProfilePM)
    func bind(actions: ProfileUI.Actions)
}

protocol ProfileInteractorOutputs: InteractorOutputs {
    var personalInformation: PublishSubject<Void> { get }
    var feedback: PublishSubject<FeedbackType> { get }
}

protocol ProfileInteractorUIOutputs {
    var updateUI: Driver<ProfilePM> { get }
}

protocol ProfileType: Interactor {
    var inputs: ProfileInteractorInputs { get }
    var outputs: ProfileInteractorOutputs { get }
    var uiOutputs: ProfileInteractorUIOutputs { get }
}

typealias ProfileStream = Observable<ProfilePM>

// MARK: - Interactor

final class ProfileInteractor: ProfileType, ProfileInteractorInputs, ProfileInteractorOutputs, ProfileInteractorUIOutputs {
    
    // MARK: - Inputs
    func configure(with presentationModel: ProfilePM) {
        presentationModelSubject.accept(presentationModel)
    }
    
    func bind(actions: ProfileUI.Actions) {
        
        actions.cell.asObservable()
            .filter { $0 == .personalInformation }
            .map { _ in () }
            .bind(to: personalInformation)
            .disposed(by: disposeBag)
        
        actions.cell.asObservable()
            .filter { $0 == .reportBug }
            .map { _ in FeedbackType.bugReport }
            .bind(to: feedback)
            .disposed(by: disposeBag)
        
        actions.cell.asObservable()
            .filter { $0 == .suggestImprovements }
            .map { _ in FeedbackType.feedback }
            .bind(to: feedback)
            .disposed(by: disposeBag)
        
        actions.cell.asObservable()
            .filter { $0 == .logout }
            .subscribe(onNext: { [weak self] _ in
                self?.performLogout()
            }).disposed(by: disposeBag)
        
    }
    
    // MARK: - UIOutputs
    lazy var updateUI: Driver<ProfilePM> = self.createUpdateUIStream()
    
    // MARK: - Scene Outputs
    var personalInformation = PublishSubject<Void>()
    var feedback = PublishSubject<FeedbackType>()
    var didFinish = PublishSubject<Void>()
    
    // MARK: - Input Output
    var inputs: ProfileInteractorInputs { return self }
    var outputs: ProfileInteractorOutputs { return self }
    var uiOutputs: ProfileInteractorUIOutputs { return self }
    
    // MARK: - Properties
    let disposeBag = DisposeBag()
    
    // MARK: - Private Properties
    private let gateway: AppGate
    private let presentationModelSubject = BehaviorRelay(value: ProfilePM())
    
    // MARK: - Lifecycle
    init(gateway: AppGate) {
        self.gateway = gateway
        
        bindProfile()
    }
    
    // MARK: - UI Output Streams
    
    private func createUpdateUIStream() -> Driver<ProfilePM> {
        return presentationModelSubject.asDriver()
    }
    
    private func bindProfile() {
        gateway.updateProfile
            .filterNil()
            .map { ProfilePM(profile: $0) }
            .bind(to: presentationModelSubject)
            .disposed(by: disposeBag)
    }
    
    private func performLogout() {
        gateway.logout()
            .subscribe(onNext: { [weak self] result in
                switch result {
                case .success:
                    self?.didFinish.onCompleted()
                default: break
                }
            }).disposed(by: disposeBag)
    }
    
}
