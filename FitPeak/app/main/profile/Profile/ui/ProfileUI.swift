import UIKit
import RxSwift
import RxCocoa

class ProfileUI: InteractableUI<ProfileInteractor>, UITableViewDelegate, UITableViewDataSource {
    
    var didFinish = PublishSubject<Void>()
    var disposedBag = DisposeBag()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // Private Properties
    private var presentationModel = ProfilePM()
    private var cellSelectedSubject = PublishSubject<ProfileItemType>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyTheme()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    private func applyTheme() {
        ProfileTheme.apply(self)
        ProfileTheme.apply(headerTitleLabel: titleLabel)
        ProfileTheme.apply(navigationController, tintColor: .navigationTintDark)
    }

    private func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
    }
    
    override func bindInteractor(interactor: ProfileInteractor) {
        interactor.uiOutputs.updateUI.drive(onNext: { [weak self] presentationModel in
            self?.presentationModel = presentationModel
            self?.updateUI(presentationModel)
        }).disposed(by: disposedBag)
    }
    
    private func updateUI(_ presentationModel: ProfilePM) {
        titleLabel.text = presentationModel.title
    }
    
    // MARK - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presentationModel.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentationModel.sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return presentationModel.rowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return presentationModel.headerHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionItem = presentationModel.sections[section]
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: presentationModel.headerHeight))
        headerView.backgroundColor = .white
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 0, width: view.frame.width, height: headerView.frame.height))
        headerLabel.font = UIFont(.bold, 12)
        headerLabel.textColor = .primaryTextColor
        headerLabel.text = sectionItem.title.uppercased()
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let sectionItem = presentationModel.sections[section]
        return sectionItem.hasFooter ? presentationModel.footerHeight : 0;
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let sectionItem = presentationModel.sections[section]
        if sectionItem.hasFooter {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: presentationModel.footerHeight))
            let footerLabel = UILabel(frame: footerView.frame)
            footerLabel.textAlignment = .center
            footerLabel.font = UIFont(.medium, 14)
            footerLabel.textColor = .primaryTextColor
            
            guard let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else { return nil }

            footerLabel.text = "Version \(version)" //(\(build))
            footerView.addSubview(footerLabel)
            return footerView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item: ProfileItemPM = presentationModel.sections[indexPath.section].items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: presentationModel.cellIdentifier, for: indexPath) as! ProfileTableViewCell
        
        cell.titleLabel.text = item.title
        cell.iconImage.image = item.icon
        cell.accessoryType = item.accessoryType
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let item: ProfileItemPM = presentationModel.sections[indexPath.section].items[indexPath.row]
        cellSelectedSubject.onNext(item.type)
    }

}

// MARK: UI Actions

extension ProfileUI {
    
    struct Actions {
        let cell: Driver<ProfileItemType>
    }
    
    var actions: Actions {
        return Actions(
            cell: cellSelectedSubject.asDriver(onErrorJustReturn: .logout)
        )
    }
}
