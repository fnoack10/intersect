import UIKit

struct ProfileTheme: ThemeProtocol {

    static func apply(_ cell: ProfileTableViewCell) {
        cell.titleLabel.font = UIFont.systemFont(ofSize: 16)
    }
    
}
