import Foundation
import Firebase
import RxSwift
import RxCocoa

typealias ResultStream<T> = Observable<Result<T, Error>>

enum AuthError: Error {
    case unauthorized
    case parseError
}

protocol AppGate {
    var currentUser: BehaviorRelay<User?> { get }
    var updateProfile: BehaviorRelay<Profile?> { get }
    var updateDiscipline: BehaviorRelay<[Discipline]> { get }
    var updateMovements: BehaviorRelay<[Movement]> { get }
    
    func addLog(with data: [String: Any], movementID: String) -> ResultStream<Void>
    func deleteLog(with log: Log, movementID: String) -> ResultStream<Void>
    func logout() -> ResultStream<Void>
}

final class AppGateway: AppGate {
    // MARK - Private Properties
    private var disposeBag = DisposeBag()
    private var database: Firestore!
    private var userID: String = ""
    
    // MARK - Public Properties
    lazy var currentUser = BehaviorRelay<User?>(value: nil)
    lazy var updateProfile = BehaviorRelay<Profile?>(value: nil)
    lazy var updateDiscipline = BehaviorRelay<[Discipline]>(value: [])
    lazy var updateMovements = BehaviorRelay<[Movement]>(value: [])
    
    init() {
        setupDatabase()
        
        bindAuthentication()
    }
    
    private func setupDatabase() {
        let settings = FirestoreSettings()
        Firestore.firestore().settings = settings
        database = Firestore.firestore()
    }
    
    private func bindAuthentication() {
        authStream()
            .filterNil()
            .subscribe(onNext: { user in
                self.userID = user.uid
                self.bindProfile()
                self.bindDisciplines()
                self.bindUserMovements()
            }).disposed(by: disposeBag)
    }
    
    private func bindDisciplines() {
        loadDisciplineTypes()
            .flatMap { $0 }
            .bind(to: updateDiscipline)
            .disposed(by: disposeBag)
    }
    
    private func bindUserMovements() {
        updateDiscipline
            .flatMap { self.loadUserMovements(for: $0) }
            .bind(to: updateMovements)
            .disposed(by: disposeBag)
    }
    
    private func bindProfile() {
        loadProfile()
            .bind(to: updateProfile)
            .disposed(by: disposeBag)
    }
    
    // MARK: - Auth
    
    private func authStream() -> Observable<User?> {
        return Observable.create { observer in
            Auth.auth().addStateDidChangeListener { (auth, user) in
                observer.onNext(user)
            }
            return Disposables.create()
        }
    }
    
    // MARK: - Profile
    
    private func loadProfile() -> Observable<Profile?> {
        return Observable.create { observer in
            let request = self.database.collection("users").document(self.userID)
            request.addSnapshotListener { (document, error) in
                guard let data = document?.data(),
                    let profile = Profile.create(with: data) else {
                        observer.onNext(nil)
                        return
                }
                observer.onNext(profile)
            }
            return Disposables.create()
        }
    }
    
    // MARK: - Disciplines
    
    private func loadDisciplineTypes() -> Observable<Observable<[Discipline]>> {
        return Observable.create { observer in
            let request = self.database.collection("disciplines").order(by: "order", descending: false)
            request.getDocuments { [unowned self] (data, _) in
                guard let documents = data?.documents else { return }
                let requests = documents.compactMap { self.loadMovementsTypes(for: $0) }
                let disciplineStream = Observable.zip(requests)
                observer.onNext(disciplineStream)
            }
            return Disposables.create()
        }
    }
            
    private func loadMovementsTypes(for snapshot: QueryDocumentSnapshot) -> Observable<Discipline> {
        return Observable.create { observer in
            let disciplineID = snapshot.documentID
            let disciplineData = snapshot.data()
            let request = self.database.collection("disciplines/\(disciplineID)/movements").order(by: "order", descending: false)
            request.getDocuments { (data, _) in
                guard let documents = data?.documents else { return }
                let movements = documents.compactMap { Movement.create(with: $0.documentID, disciplineId: disciplineID, data: $0.data()) }
                guard let discipline = Discipline.create(with: disciplineID, data: disciplineData , movements: movements) else { return }
                observer.onNext(discipline)
            }
            return Disposables.create()
        }
    }
    
    // MARK: - Logs
    
    func loadUserMovements(for disciplines: [Discipline]) -> Observable<[Movement]> {
        return Observable.create { observer in
            let movements = disciplines.flatMap { $0.movements }
            movements.forEach { $0.updateLogsWithStream(self.loadUserLogs(with: self.userID, movement: $0))}
            observer.onNext(movements)
            return Disposables.create()
        }
    }

    func loadUserLogs(with userID: String, movement: Movement) -> Observable<[Log]> {
        return Observable.create { observer in
            let request = self.database.collection("users/\(userID)/movements/\(movement.id)/logs")
            request.order(by: "creationDate", descending: true).addSnapshotListener { document, _ in
                guard let documents = document?.documents else { return }
                let logs = documents.compactMap { Log.create(with: $0.documentID, data: $0.data(), movement: movement) }
                observer.onNext(logs)
            }
            return Disposables.create()
        }
    }
    
    // MARK: - Input
    
    func addLog(with data: [String: Any], movementID: String) -> ResultStream<Void> {
        return Observable.create { observer in
            _ = self.database.collection("users/\(self.userID)/movements/\(movementID)/logs")
                .addDocument(data: data) { error in
                    if let error = error {
                        observer.onError(error)
                    } else {
                        observer.onNext(.success(()))
                    }
            }
            return Disposables.create()
        }
    }
    
    // MARK: - Delete
    
    func deleteLog(with log: Log, movementID: String) -> ResultStream<Void> {
        return Observable.create { observer in
            _ = self.database.collection("users/\(self.userID)/movements/\(movementID)/logs").document(log.id).delete() { error in
                    if let error = error {
                        observer.onError(error)
                    } else {
                        observer.onNext(.success(()))
                    }
            }
            return Disposables.create()
        }
    }
    
    // MARK: - Logout
    
    func logout() -> ResultStream<Void> {
        return Observable.create { observer in
            do {
                try Auth.auth().signOut()
                observer.onNext(.success(()))
            } catch let error {
                observer.onNext(.failure(error))
            }
            return Disposables.create()
        }
    }
    
}
