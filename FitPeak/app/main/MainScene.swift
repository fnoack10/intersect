import Foundation
import RxSwift
import Firebase

final class MainScene: Scene {
    
    // MARK: - Properties
    var didFinish = PublishSubject<Void>()
    var children: [Scene] = []
    var disposeBag = DisposeBag()
    
    // MARK: - Private Properties
    private let gateway: AppGate
    private let navigationContext: NavigationControllerContext
    
    // MARK: - Lifecycle
    init(gateway: AppGate, navigationContext: NavigationControllerContext) {
        self.gateway = gateway
        self.navigationContext = navigationContext
    }
    
    func start() {
        launchOverviewUI()
        setupTabBar()
    }
    
    private func setupTabBar() {
        let tabBarItem = UITabBarItem(
            title: "Progress",
            image: Asset.TabBar.personalBest.image,
            selectedImage: Asset.TabBar.personalBest.image)
        tabBarItem.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        navigationContext.navigationController.tabBarItem = tabBarItem
    }

}

extension MainScene {
    
    private func launchOverviewUI() {
        let ui = OverviewUI.create(scene: StoryboardScene.Overview.overviewUI) { overviewUI in
            guard let overviewUI = overviewUI as? OverviewUI else { fatalError() }
            
            let interactor = OverviewInteractor(gateway: self.gateway)
            interactor.inputs.bind(actions: overviewUI.actions)
            
            interactor.outputs.movementSelected
                .subscribe(onNext: { [weak self] movement in
                    self?.launchMovementOverview(with: movement)
                }).disposed(by: overviewUI.disposeBag)
            
            return interactor
        }
        navigationContext.push(ui, animated: false, mode: .topmost, completion: nil)
    }
    
    private func launchMovementOverview(with movement: Movement) {
        let ui = MovementOverviewUI.create(scene: StoryboardScene.MovementOverview.movementOverviewUI) { movementOverviewUI in
            guard let movementOverviewUI = movementOverviewUI as? MovementOverviewUI else { fatalError() }
            
            let interactor = MovementInteractor(gateway: self.gateway, movement: movement)
            interactor.inputs.bind(actions: movementOverviewUI.actions)
            
            interactor.outputs.back
                .subscribe(onNext: { [weak self] _ in
                    self?.navigationContext.pop()
            }).disposed(by: movementOverviewUI.disposeBag)
            
            interactor.outputs.add
                .subscribe(onNext: { [weak self] _ in
                    self?.launchInputScene(with: movement)
            }).disposed(by: movementOverviewUI.disposeBag)
            
            return interactor
        }
        navigationContext.push(ui)
    }
    
    private func launchInputScene(with movement: Movement) {
        let modalContext = ModalContext(presenter: self.navigationContext.navigationController)
        let ui = InputUI.create(scene: StoryboardScene.Input.inputUI) { inputUI in
            guard let inputUI = inputUI as? InputUI else { fatalError() }
            
            let interactor = InputInteractor(gateway: self.gateway, movement: movement)
            interactor.inputs.bind(actions: inputUI.actions)
            
            interactor.outputs.close
                .subscribe(onNext: { _ in
                    modalContext.dismiss()
            }).disposed(by: inputUI.disposeBag)
            
            return interactor
        }
        
        modalContext.present(ui)
    }
    
}
