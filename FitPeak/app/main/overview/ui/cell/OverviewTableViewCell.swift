import UIKit

class OverviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var valueView: UIView!
    @IBOutlet weak var valueLabel: UILabel!
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        applyTheme()
        setupUI()
    }
    
    private func applyTheme() {
        OverviewTheme.apply(titleLabel: titleLabel)
        OverviewTheme.apply(subtitleLabel: subtitleLabel)
        OverviewTheme.apply(valueLabel: valueLabel)
        OverviewTheme.apply(valueView: valueView)
    }
    
    private func setupUI() {
        self.accessoryType = .disclosureIndicator
    }
    
    func configure(_ movement: Movement) {
        titleLabel.text =   movement.label
        subtitleLabel.text = movement.formattedMostRecentLog
        valueLabel.text = movement.formattedBestScore
        valueView.isHidden = movement.isHidden
    }
}


