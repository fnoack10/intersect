import UIKit

struct OverviewTheme: ThemeProtocol {
    
    static func apply(_ viewController: OverviewUI) {
        viewController.view.backgroundColor = .backgroundColorPrimary
        viewController.tableView.backgroundColor = .backgroundColorSecondary
    }
    
    static func apply(titleLabel: UILabel) {
        titleLabel.textColor = .primaryTextColor
        titleLabel.font = UIFont(.semibold, 18)
    }
    
    static func apply(subtitleLabel: UILabel) {
        subtitleLabel.textColor = .detailTextColor
        subtitleLabel.font = UIFont(.semibold, 12)
    }
    
    static func apply(valueLabel: UILabel) {
        valueLabel.textColor = .lightTextColor
        valueLabel.font = UIFont(.semibold, 16)
    }
    
    static func apply(valueView: UIView) {
        valueView.backgroundColor = .main
        valueView.layer.cornerRadius = 8.0
    }
    
}
