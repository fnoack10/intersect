import UIKit
import RxSwift
import RxCocoa

class OverviewUI: InteractableUI<OverviewInteractor>, UITableViewDelegate, UITableViewDataSource {
    
    var disciplineSelectedSubject = PublishSubject<Int?>()
    var movementSelectedSubject = PublishSubject<Int?>()
    var disposedBag = DisposeBag()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectorContainerView: UIStackView!
    
    lazy private var filterSelectorView: FilterSelectorView = createFilterSelector()
    lazy private var presentationModel: OverviewPM = OverviewPM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyTheme()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    private func applyTheme() {
        OverviewTheme.apply(self)
        OverviewTheme.apply(headerTitleLabel: titleLabel)
        OverviewTheme.apply(navigationController, tintColor: .navigationTintDark)
    }
    
    private func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        selectorContainerView.addArrangedSubview(filterSelectorView)
    }
    
    override func bindInteractor(interactor: OverviewInteractor) {
        interactor.uiOutputs.updateUI.drive(onNext: { [weak self] presentationModel in
            self?.presentationModel = presentationModel
            self?.updateUI(presentationModel)
        }).disposed(by: disposedBag)
    }
    
    private func updateUI(_ presentationModel: OverviewPM) {
        title = presentationModel.title
        titleLabel.text = presentationModel.title
        
        guard !presentationModel.disciplines.isEmpty else { return }
        let sections = presentationModel.disciplines.map { $0.label }
        filterSelectorView.configure(with: sections)
        tableView.reloadData()
    }
    
    // MARK - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presentationModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentationModel.movements.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return presentationModel.rowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return presentationModel.headerHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: presentationModel.cellIdentifier, for: indexPath) as! OverviewTableViewCell
        let movement = presentationModel.movements[indexPath.row]
        cell.configure(movement)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        movementSelectedSubject.onNext(indexPath.row)
    }
    
    // MARK: - Selector
    
    private func createFilterSelector() -> FilterSelectorView {
        let filterSelectorView = FilterSelectorView.fromNib()
        filterSelectorView.filterSelectorDelegate = self
        return filterSelectorView
    }
    
}

extension OverviewUI: FilterSelectorDelegate {
    
    func didSelect(index: Int) {
        disciplineSelectedSubject.onNext(index)
    }
    
}

// MARK: UI Actions

extension OverviewUI {
    
    struct Actions {
        let movementSelected: Driver<Int>
        let disciplineSelected: Driver<Int>
    }
    
    var actions: Actions {
        return Actions(
            movementSelected: movementSelectedSubject
                .compactMap { $0 }
                .asDriver(onErrorJustReturn: 0),
            disciplineSelected: disciplineSelectedSubject
                .compactMap { $0 }
                .asDriver(onErrorJustReturn: 0)
        )
    }
}
