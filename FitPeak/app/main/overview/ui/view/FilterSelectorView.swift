import UIKit

protocol FilterSelectorDelegate {
    func didSelect(index: Int)
}

final class FilterSelectorView: UIScrollView, NibLoadableView {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var separatorHeightConstraint: NSLayoutConstraint!
    
    var filterSelectorDelegate: FilterSelectorDelegate?
    var selectedSection: Int = 0 {
        didSet {
            filterSelectorDelegate?.didSelect(index: selectedSection)
        }
    }
    
    private var selectionMarkerView: UIView = UIView()
    private var sections: [String] = []
    private var tabs: [UIButton] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupUI()
    }
    
    private func setupUI() {
        backgroundColor = .backgroundColorPrimary
        separatorHeightConstraint.constant = 0.5
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        stackView.isLayoutMarginsRelativeArrangement = true
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        selectionMarkerView.backgroundColor = .main
        selectionMarkerView.layer.cornerRadius = 1.0
        self.addSubview(selectionMarkerView)
    }
    
    func configure(with sections: [String]) {
        // Only update if sections are different
        guard sections != self.sections else { return }
        
        self.sections = sections
        activityIndicator.stopAnimating()
        setupSections()
        
        updateSelectedTab(tabs[0])
    }
    
    private func setupSections() {
        for i in 0..<sections.count {
            let name = sections[i]
            let button = UIButton(type: .custom)
            let title = name.uppercased()
            
            button.tag = i
            button.isSelected = (i == selectedSection)
            button.titleLabel?.font = UIFont(.bold, 12)
            button.setTitle(title, for: .normal)
            button.setTitleColor(.primaryTextColor, for: .selected)
            button.setTitleColor(.secondaryTextColor, for: .normal)
            button.addTarget(self, action: #selector(selected(sender:)), for: .touchUpInside)
            tabs.append(button)
            
            stackView.addArrangedSubview(button)
        }
        
        layoutIfNeeded()
        selectionMarkerView.frame = CGRect(x: 0, y: 0, width: 0, height: 3)
        selectionMarkerView.center = CGPoint(x: 0, y: frame.size.height-1)
    }
    
    private func updateSelectedTab(_ tab: UIButton) {
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: { [weak self] in
            self?.selectionMarkerView.frame = CGRect(x: 0, y: 0, width: tab.frame.size.width, height: 3)
            self?.selectionMarkerView.center = CGPoint(x: tab.center.x, y: tab.frame.size.height-1)
            guard let x = self?.getContentyOffset(tab: tab) else { return }
            self?.setContentOffset( CGPoint(x: x, y: 0.0), animated: true)
            }, completion: nil)
    }
    
    private func getContentyOffset(tab: UIButton) -> CGFloat? {
        let screenWidth = frame.width
        let stackViewWidth = stackView.frame.width
        let difference = stackViewWidth-screenWidth
        
        if difference >= 0 {
            if tab.tag == 0 {
                return 0.0
            } else if tab.tag == tabs.count-1 {
                return difference
            }
        }
        return nil
    }
    
    @objc private func selected(sender: UIButton) {
        selectedSection = sender.tag
        sender.isSelected = !sender.isSelected
        
        for tab in tabs {
            tab.isSelected = tab.tag == sender.tag
        }
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: { [weak self] in
            self?.updateSelectedTab(sender)
        }, completion: nil)
    }
    
}
