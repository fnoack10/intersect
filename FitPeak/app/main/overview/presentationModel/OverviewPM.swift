import Foundation
import UIKit

struct OverviewPM {
    
    var title: String = "Progress"
    var numberOfSections: Int = 1
    var headerHeight: CGFloat = 20.0
    var rowHeight: CGFloat = 60.0
    var cellIdentifier: String = "OverviewTableViewCell"
    var disciplines: [Discipline] = []
    var movements: [Movement] = []

    init() {
    }
    
    init(movements: [Movement], disciplines: [Discipline]) {
        self.movements = movements
        self.disciplines = disciplines
    }

}
