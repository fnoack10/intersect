import Foundation
import RxSwift
import RxCocoa

protocol OverviewInteractorInputs {
    func bind(actions: OverviewUI.Actions)
}

protocol OverviewInteractorOutputs: InteractorOutputs {
    var movementSelected: PublishSubject<Movement> { get }
}

protocol OverviewInteractorUIOutputs {
    var updateUI: Driver<OverviewPM> { get }
}

protocol OverviewType: Interactor {
    var inputs: OverviewInteractorInputs { get }
    var outputs: OverviewInteractorOutputs { get }
    var uiOutputs: OverviewInteractorUIOutputs { get }
}

typealias OverviewStream = Observable<OverviewPM>

// MARK: - Interactor

final class OverviewInteractor: OverviewType, OverviewInteractorInputs, OverviewInteractorOutputs, OverviewInteractorUIOutputs {
    
    // MARK: - Inputs
    func bind(actions: OverviewUI.Actions) {
        actions.movementSelected.asObservable()
            .compactMap { $0 }
            .withLatestFrom(disciplineSelectedSubject) { ($0, $1)}
            .withLatestFrom(disciplinesSubject) { ($0.0, $0.1, $1)}
            .map { selectedMovement, selectedDiscipline, disciplines in
                let discipline = disciplines[selectedDiscipline]
                return discipline.movements[selectedMovement]
            }.bind(to: movementSelected)
            .disposed(by: disposeBag)
        
        actions.disciplineSelected.asObservable()
            .bind(to: disciplineSelectedSubject)
            .disposed(by: disposeBag)
    }

    // MARK: - UIOutputs
    lazy var updateUI: Driver<OverviewPM> = self.createUpdateUIStream()
    
    // MARK: - Internal Streams
    var disciplinesSubject = PublishSubject<[Discipline]>()
    var disciplineSelectedSubject: BehaviorSubject<Int> = BehaviorSubject<Int>(value: 0)
    
    // MARK: - Scene Outputs
    var movementSelected: PublishSubject<Movement> = PublishSubject<Movement>()
    var didFinish = PublishSubject<Void>()

    // MARK: - Input Output
    var inputs: OverviewInteractorInputs { return self }
    var outputs: OverviewInteractorOutputs { return self }
    var uiOutputs: OverviewInteractorUIOutputs { return self }

    // MARK: - Properties
    let disposeBag = DisposeBag()

    // MARK: - Private Properties
    private let gateway: AppGate
    private let presentationModelSubject = BehaviorRelay(value: OverviewPM())
    
    // MARK: - Lifecycle
    init(gateway: AppGate) {
        self.gateway = gateway
        
        bindDisciplines()
        bindMovements()
    }
    
    private func bindDisciplines() {
        gateway.updateDiscipline
            .bind(to: disciplinesSubject)
            .disposed(by: disposeBag)
    }
    
    private func reloadStream() -> Observable<Void> {
        let movementUpdate = gateway.updateMovements
            .flatMap { Observable.merge($0.compactMap { $0.needsUpdate }) }
        
        let disciplineSelectedChanged = disciplineSelectedSubject.asObserver()
            .map { _ in () }
        
        return Observable.merge(movementUpdate, disciplineSelectedChanged)
    }
    
    private func bindMovements() {
        reloadStream()
            .withLatestFrom(gateway.updateMovements) { $1 }
            .withLatestFrom(disciplineSelectedSubject) {($0, $1)}
            .withLatestFrom(disciplinesSubject) {($0.0, $0.1, $1)}
            .map { self.createOverviewPM($0, selectedIndex: $1, disciplines: $2) }
            .bind(to: presentationModelSubject)
            .disposed(by: disposeBag)
    }
    
    // MARK: - Stream Operations
    
    private func createOverviewPM(_ movements: [Movement], selectedIndex: Int, disciplines: [Discipline]) -> OverviewPM {
        let discipline = disciplines[selectedIndex]
        let filteredMovements = movements.filter { discipline.movements.contains($0) }
        return OverviewPM(movements: filteredMovements, disciplines: disciplines)
    }
    
    // MARK: - UI Output Streams
    
    private func createUpdateUIStream() -> Driver<OverviewPM> {
        return presentationModelSubject.asDriver()
    }
    
}
