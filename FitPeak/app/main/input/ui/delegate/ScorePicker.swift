import Foundation
import UIKit

// MARK: UIPickerView
class ScorePicker: NSObject, UIPickerViewDataSource, UIPickerViewDelegate {
    
    let model: InputPickerPM
    let didSelectRow: (Int) -> Void
    
    init(model: InputPickerPM, didSelectRow: @escaping (Int) -> Void) {
        self.model = model
        self.didSelectRow = didSelectRow
    }
    
    func setupParameters(for picker: UIPickerView, containerView: UIView, inputPM: InputPM) {
        updatePickerLabels(picker: picker, containerView: containerView, components: picker.numberOfComponents, unitLabels: inputPM.unitLabels)
        updatePickerToBestScore(picker: picker, inputPM: inputPM)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return model.components.count * 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (component % 2 == 0) ? model.components[component/2].numberOfRowsInComponent : 1
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return model.rowHeight
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel: UILabel = UILabel()
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .right
        let string = (component % 2 == 0) ? row.description : ""
        pickerLabel.attributedText = NSAttributedString(string: string, attributes: [.font: UIFont(.regular, 40),
                                                                                     .foregroundColor: UIColor.primaryTextColor,
                                                                                     .paragraphStyle: paragraphStyle])
        return pickerLabel
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        var totalScore: Int = 0
        
        for i in 0...model.components.count-1 {
            let selectedComponent = model.components[i]
            let index = i*2
            let selectedRow = pickerView.selectedRow(inComponent: index)
            guard let label = pickerView.view(forRow: selectedRow, forComponent: index) as? UILabel,
                let scoreString = label.text,
                let score = Int(scoreString) else { return }
            
            switch selectedComponent.type {
            case .hours:
                totalScore += score*60*60
            case .minutes:
                totalScore += score*60
            default: totalScore += score
            }
            
        }
        
        didSelectRow(totalScore)
    }
    
    func updatePickerLabels(picker: UIPickerView, containerView: UIView, components: Int, unitLabels: [String]) {
        var pickerlabels:  [Int: UILabel] = [:]
        let labelCount = components/2
        for n in 1...labelCount {
            let index = n*2-1
            let label = UILabel()
            label.text = unitLabels[n-1]
            pickerlabels.updateValue(label, forKey: index)
        }
        picker.setPickerLabels(labels: pickerlabels, containedView: containerView)
    }
    
    func updatePickerToBestScore(picker: UIPickerView, inputPM: InputPM) {
        switch model.type {
        case .weight:
            picker.selectRow(inputPM.bestScore, inComponent: 0, animated: false)
        case .time:
            let timeScore = inputPM.bestScoreTimeComponents
            picker.selectRow(timeScore.0 , inComponent: 0, animated: false)
            picker.selectRow(timeScore.1, inComponent: 2, animated: false)
            picker.selectRow(timeScore.2, inComponent: 4, animated: false)
        case .reps:
            picker.selectRow(inputPM.bestScore, inComponent: 0, animated: false)
        }
    }
}
