//
//  ScoreSelectionTableViewCell.swift
//  Intersect
//
//  Created by Franco Noack on 23.12.18.
//  Copyright © 2018 Intersect. All rights reserved.
//

import UIKit

class ScoreSelectionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var metricLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
