import UIKit
import RxSwift
import RxCocoa

class InputUI: InteractableUI<InputInteractor>, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    var didFinish = PublishSubject<Void>()
    var addInput = PublishSubject<[String: Any]>()
    var disposedBag = DisposeBag()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var datePickerView: DatePickerView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var datePickerBottomConstraint: NSLayoutConstraint!
    
    private var presentationModel = InputPM()
    private var scorePicker: ScorePicker?
    private var countLabel: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyTheme()
        setupUI()
        bindActions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterKeyboardNotifications()
    }
    
    fileprivate func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    fileprivate func deregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func applyTheme() {
        InputTheme.apply(headerTitleLabel: titleLabel)
        InputTheme.apply(datePickerView: datePickerView)
        InputTheme.apply(addButton)
    }
    
    private func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
        datePickerView.cancelButton.addTarget(self, action: #selector(cancelDateSelection), for: .touchUpInside)
        datePickerView.saveButton.addTarget(self, action: #selector(didSelectDate), for: .touchUpInside)
        activityIndicator.hidesWhenStopped = true
    }
    
    private func bindActions() {
        addButton.rx.tap.asObservable()
            .do(onNext: { _ in self.startLoading() })
            .map { self.presentationModel.export() }
            .bind(to: addInput)
            .disposed(by: disposedBag)
    }
    
    override func bindInteractor(interactor: InputInteractor) {
        interactor.uiOutputs.updateUI.drive(onNext: { [weak self] presentationModel in
            self?.presentationModel = presentationModel
            self?.updateUI(presentationModel)
        }).disposed(by: disposedBag)
    }
    
    private func updateUI(_ presentationModel: InputPM) {
        titleLabel.text = presentationModel.title
        addButton.setTitle(presentationModel.actionTitle, for: .normal)
        datePickerView.cancelButton.setTitle(presentationModel.cancelTitle, for: .normal)
        datePickerView.saveButton.setTitle(presentationModel.saveTitle, for: .normal)
        datePickerView.setup(date: presentationModel.creationDate, maximumDate: Date())
    
        tableView.reloadData()
    }
    
    // MARK - Keyboard
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo!
        let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            self.tableViewBottomConstraint.constant = keyboardHeight
            self.view.layoutIfNeeded()
            self.tableView.scrollToBottom()
        }, completion: nil)
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseInOut, animations: {
            self.tableViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // MARK - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presentationModel.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return presentationModel.sections[indexPath.section].rowHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return presentationModel.headerHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionItem = presentationModel.sections[section]
        return TableViewHeader.createSecondary(title: sectionItem.title, height: presentationModel.headerHeight, view: view)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let inputFieldPM: InputFieldPM = presentationModel.sections[indexPath.section]
        switch inputFieldPM.type {
        case .notes:
            let cell = tableView.dequeueReusableCell(withIdentifier: inputFieldPM.cellIdentifier, for: indexPath) as! NotesTableViewCell
            cell.textView.delegate = self
            countLabel = cell.countLabel
            validateInput(for: cell.textView)
            bindInputLimit(for: cell.textView)
            return cell
        case .weight, .time, .reps:
            let cell = tableView.dequeueReusableCell(withIdentifier: inputFieldPM.cellIdentifier, for: indexPath) as! ScoreTableViewCell
            guard let pickerType = inputFieldPM.type.pickerType else { return cell }
            let inputPickerPM = InputPickerPM(type: pickerType)
            if scorePicker == nil {
                scorePicker = ScorePicker(model: inputPickerPM, didSelectRow: didSelectRow(_:))
                cell.picker.delegate = scorePicker
                cell.picker.dataSource = scorePicker
                scorePicker?.setupParameters(for: cell.picker, containerView: cell.contentView, inputPM: presentationModel)
            }
            return cell
        case .date:
            let cell = tableView.dequeueReusableCell(withIdentifier: inputFieldPM.cellIdentifier, for: indexPath) as! DateTableViewCell
            cell.titleLabel.text = presentationModel.creationDate.translateDate()
            cell.accessoryType = .disclosureIndicator
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        showDatePicker()
    }
    
    func didSelectRow(_ score: Int) {
        presentationModel.score = score
    }
    
    // MARK: - TextView
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    func textViewDidChange(_ textView: UITextView) {
        validateInput(for: textView)
        presentationModel.note = textView.text
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return true
    }
    
    // MARK - Activity
    
    private func startLoading() {
        activityIndicator.startAnimating()
        addButton.isEnabled = false
    }
    
    private func endLoading() {
        activityIndicator.stopAnimating()
        addButton.isEnabled = true
    }
    
    // MARK: - Helpers
    
    func bindInputLimit(for textView: UITextView) {
        textView.rx.didChange
            .map { textView.text }
            .map { self.characters(from: $0) }
            .map { !(self.presentationModel.countLimit < $0.count) }
            .bind(to: addButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }
    
    private func characters(from string: String) -> String {
        return string.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    private func validateInput(for textView: UITextView) {
        guard let label = countLabel else { return }
        let currentCount = presentationModel.countLimit - characters(from: textView.text).count
        label.text = currentCount.description
        
        let validCount = currentCount >= 0
        InputTheme.apply(label, isValid: validCount)
    }
    
}

// MARK: DatePicker
extension InputUI {
    @objc func cancelDateSelection() {
        hideDatePicker()
    }
    
    @objc func didSelectDate() {
        hideDatePicker()
        presentationModel.creationDate = datePickerView.datePicker.date
        
        let section = presentationModel.sections.firstIndex{ $0.type == .date }
        guard let index = section else { return }
        tableView.reloadSections([index], with: .automatic)
    }
    
    func showDatePicker() {
        self.datePickerBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hideDatePicker() {
        self.datePickerBottomConstraint.constant = datePickerView.frame.height
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
}

// MARK: UI Actions

extension InputUI {
    
    struct Actions {
        let close: Driver<Void>
        let add: Driver<[String: Any]>
    }
    
    var actions: Actions {
        return Actions(
            close: closeButton.rx.tap.asDriver(onErrorJustReturn: ()),
            add: addInput.asDriver(onErrorJustReturn: [:])
        )
    }
}



