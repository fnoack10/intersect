import UIKit

struct InputTheme: ThemeProtocol {
        
    static func apply(dateLabel: UILabel) {
        dateLabel.textColor = .primaryTextColor
        dateLabel.font = UIFont(.medium, 16)
    }
    
    static func apply(countLabel: UILabel) {
        countLabel.textColor = .primaryTextColor
        countLabel.font = UIFont(.regular, 16)
    }
    
    static func apply(datePickerView: UIView) {
        datePickerView.layer.borderWidth = 0.5
        datePickerView.layer.borderColor = UIColor.lineGray.cgColor
    }

    static func apply(cancelButton: UIButton) {
        cancelButton.titleLabel?.textAlignment = .left
        cancelButton.titleLabel?.font = UIFont(.bold, 16)
        cancelButton.setTitleColor(.secondaryTextColor, for: .normal)
    }
    
    static func apply(actionButton: UIButton) {
        actionButton.titleLabel?.textAlignment = .right
        actionButton.titleLabel?.font = UIFont(.bold, 16)
        actionButton.setTitleColor(.activeTextColor, for: .normal)
    }
    
    static func apply(_ countLabel: UILabel, isValid: Bool) {
        countLabel.textColor = isValid ? .primaryTextColor : .errorTextColor
    }
        
}
