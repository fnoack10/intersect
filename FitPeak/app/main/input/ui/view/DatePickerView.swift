//
//  DatePickerView.swift
//  Intersect
//
//  Created by Franco Noack on 24.03.19.
//  Copyright © 2019 Intersect. All rights reserved.
//

import Foundation
import UIKit

class DatePickerView: UIView {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        applyTheme()
    }
    
    func applyTheme() {
        InputTheme.apply(cancelButton: cancelButton)
        InputTheme.apply(actionButton: saveButton)
    }
    
    func setup( date: Date, maximumDate: Date) {
        datePicker.datePickerMode = .date
        datePicker.date = date
        datePicker.maximumDate = maximumDate
    }
    
}
