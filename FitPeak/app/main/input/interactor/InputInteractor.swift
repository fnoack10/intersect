import Foundation
import RxSwift
import RxCocoa

protocol InputInteractorInputs {
    func bind(actions: InputUI.Actions)
}

protocol InputInteractorOutputs: InteractorOutputs {
    var close: PublishSubject<Void> { get }
    var add: PublishSubject<[String: Any]> { get }
}

protocol InputInteractorUIOutputs {
    var updateUI: Driver<InputPM> { get }
}

protocol InputProtocolType: Interactor {
    var inputs: InputInteractorInputs { get }
    var outputs: InputInteractorOutputs { get }
    var uiOutputs: InputInteractorUIOutputs { get }
}

// MARK: - Interactor

final class InputInteractor: InputProtocolType, InputInteractorInputs, InputInteractorOutputs, InputInteractorUIOutputs {
    
    // MARK: - Inputs
    func bind(actions: InputUI.Actions) {
        actions.close.asObservable()
            .bind(to: close)
            .disposed(by: disposeBag)
        
        actions.add.asObservable()
            .flatMap { self.gateway.addLog(with: $0, movementID: self.movement.id) }
            .subscribe(onNext: { result in
                switch result {
                case .success:
                    self.close.onNext(())
                case .failure:
                    print("failure")
                }
            }).disposed(by: disposeBag)
    }

    // MARK: - UIOutputs
    lazy var updateUI: Driver<InputPM> = self.createUpdateUIStream()

    // MARK: - Scene Outputs
    var close: PublishSubject<Void> = PublishSubject<Void>()
    var add: PublishSubject<[String: Any]> = PublishSubject<[String: Any]>()
    var didFinish = PublishSubject<Void>()

    // MARK: - Input Output
    var inputs: InputInteractorInputs { return self }
    var outputs: InputInteractorOutputs { return self }
    var uiOutputs: InputInteractorUIOutputs { return self }

    // MARK: - Properties
    let disposeBag = DisposeBag()

    // MARK: - Private Properties
    private let gateway: AppGate
    private let movement: Movement
    private let presentationModelSubject = BehaviorRelay(value: InputPM())
    
    // MARK: - Lifecycle
    init(gateway: AppGate, movement: Movement) {
        self.gateway = gateway
        self.movement = movement
        
        bindMovement()
    }
    
    private func bindMovement() {
        movement.needsUpdate
            .map { InputPM(movement: self.movement) }
            .bind(to: presentationModelSubject)
            .disposed(by: disposeBag)
    }
    
    // MARK: - UI Output Streams
    
    private func createUpdateUIStream() -> Driver<InputPM> {
        return presentationModelSubject.asDriver()
    }
    
}
