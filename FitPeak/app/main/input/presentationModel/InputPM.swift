//
//  CreateLogPM.swift
//  Intersect
//
//  Created by Franco Noack on 23.12.18.
//  Copyright © 2018 Intersect. All rights reserved.
//

import Foundation
import UIKit

struct InputPM {
    
    let title: String = "New Score"
    let actionTitle: String = "Add"
    let cancelTitle: String = "Cancel"
    let saveTitle: String = "Save"
    var sections: [InputFieldPM] = []
    let headerHeight: CGFloat = 40.0
    var creationDate: Date = Date()
    var score: Int = 0
    var note: String = ""
    var userUnits: String = ""
    var bestScore: Int = 0
    var bestScoreTimeComponents: (Int, Int, Int) = (0,0,0)
    var pickerType: PickerType?
    let countLimit: Int = 120
    
    init() {}
    
    init(movement: Movement) {
        sections = [InputFieldPM(type: .date),
                    InputFieldPM(type: movement.type),
                    InputFieldPM(type: .notes)]
        pickerType = movement.type.pickerType
        userUnits = movement.userUnits()
        bestScore = movement.bestScore
        bestScoreTimeComponents = movement.bestScoreTimeComponents
    }
    
    var unitLabels: [String] {
        guard let type = self.pickerType else { return [] }
        switch type {
        case .weight: return [userUnits]
        case .time: return ["hrs", "min", "sec"]
        case .reps: return ["reps"]
        }
    }
    
    func export() -> [String: Any] {
        return ["creationDate": creationDate,
                "score": score,
                "note": note]
    }
}
