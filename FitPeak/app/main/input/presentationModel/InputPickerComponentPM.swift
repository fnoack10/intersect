import Foundation

enum PickerComponentType {
    case generic
    case hours
    case minutes
    case seconds
}

struct PickerComponentPM {
    let type: PickerComponentType
    let numberOfRowsInComponent: Int
    
    init(_ type: PickerComponentType) {
        self.type = type
        
        switch type {
        case .generic:
            numberOfRowsInComponent = 2501
        case .hours:
            numberOfRowsInComponent = 24
        case .minutes:
            numberOfRowsInComponent = 60
        case .seconds:
            numberOfRowsInComponent = 60
        }
    }
    
}
