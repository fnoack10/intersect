import Foundation
import UIKit

enum InputDirection {
    case max
    case min
}

enum InputFieldType: String {
    case date
    case weight
    case time
    case reps
    case notes
    
    var direction: InputDirection {
        switch self {
        case .weight: return .max
        case .time: return .min
        case .reps: return .max
        default: return .max
        }
    }
    
    var pickerType: PickerType? {
        switch self {
        case .weight: return .weight
        case .time: return .time
        case .reps: return .reps
        default: return nil
        }
    }
}

struct InputFieldPM {
    
    var title: String
    var cellIdentifier: String
    var type: InputFieldType
    var initialValue: Int
    var rowHeight: CGFloat
    var accessoryType: UITableViewCell.AccessoryType = .none
    
    init(type: InputFieldType, initialValue: Int = 0) {
        self.type = type
        self.initialValue = initialValue
        
        switch type {
        case .date:
            title = "Date"
            rowHeight = 40.0
            cellIdentifier = "DateTableViewCell"
        case .weight:
            title = "Weight"
            rowHeight = 200.0
            cellIdentifier = "ScoreTableViewCell"
        case .time:
            title = "Time"
            rowHeight = 200.0
            cellIdentifier = "ScoreTableViewCell"
        case .reps:
            title = "Reps"
            rowHeight = 200.0
            cellIdentifier = "ScoreTableViewCell"
        case .notes:
            title = "Notes"
            rowHeight = 160.0
            cellIdentifier = "NotesTableViewCell"
        }
    }
    
}
