import Foundation
import UIKit

enum PickerType {
    case weight
    case time
    case reps
}

struct InputPickerPM {
    
    let type: PickerType
    let components: [PickerComponentPM]
    
    let rowHeight: CGFloat = 50
    
    init(type: PickerType) {
        self.type = type
        self.components = InputPickerPM.createComponentsForType(type)
    }
    
    static func createComponentsForType(_ type: PickerType) -> [PickerComponentPM] {
        switch type {
        case .weight:
            return [PickerComponentPM(.generic)]
        case .time:
            return [PickerComponentPM(.hours),
                    PickerComponentPM(.minutes),
                    PickerComponentPM(.seconds)]
        case .reps:
            return [PickerComponentPM(.generic)]
        }
    
    }
    
}
