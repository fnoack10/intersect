import Foundation
import RxSwift
import Firebase

final class AppScene: Scene {
    // MARK: - Properties
    var didFinish = PublishSubject<Void>()
    var children: [Scene] = []
    var disposeBag = DisposeBag()
    
    // MARK: - Private Properties
    private let context: WindowContext
    private lazy var authGateway: AuthGate = self.createAuthGateway()

    private func createAuthGateway() -> AuthGate {
        return AuthGateway()
    }
    
    // MARK: - Lifecycle
    init(context: WindowContext) {
        self.context = context
    }
    
    func start() {
        if authGateway.isAuthenticated {
            launchMainScene()
        } else {
            launchAuthScene()
        }
    }
}

// MARK: - Scenes
extension AppScene {
    
    private func launchAuthScene() {
        let navContext = NavigationControllerContext(parent: context) { navigationController in
            navigationController.modalPresentationCapturesStatusBarAppearance = true
            navigationController.modalPresentationStyle = .overFullScreen
            navigationController.modalTransitionStyle = .crossDissolve
            navigationController.isNavigationBarHidden = true
        }

        let authScene = AuthScene(gateway: authGateway, context: navContext)
        
        authScene.didFinish
            .subscribe(onCompleted: { [weak self] in
                self?.removeAllChildren()
                self?.launchMainScene()
            }).disposed(by: disposeBag)
        
        authScene.start()
        addChild(authScene)
    }
    
    private func launchMainScene() {
        let tabScene = TabScene(gateway: AppGateway(), tabContext: TabContext(parent: context))
        tabScene.start()
        addChild(tabScene)
        
        tabScene.didFinish
            .subscribe(onCompleted: { [weak self] in
                self?.removeAllChildren()
                self?.launchAuthScene()
            }).disposed(by: disposeBag)
    }
    
}
