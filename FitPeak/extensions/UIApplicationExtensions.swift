import Foundation
import UIKit

extension UIApplication {
    /// Attempts to determine the topmost currently visible view controller.
    // swiftlint:disable cyclomatic_complexity
    class func topViewController(in viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController,
                                 prefersMaster: Bool = false) -> UIViewController? {
        /// For NavigationControllers, return the currently visible view controller.
        if let navigationController = viewController as? UINavigationController {
            return topViewController(in: navigationController.visibleViewController, prefersMaster: prefersMaster)
        }

        /// For TabBarControllers, return the currently selected view controller.
        if
            let tabBarController = viewController as? UITabBarController,
            let selectedViewController = tabBarController.selectedViewController {
                return topViewController(in: selectedViewController, prefersMaster: prefersMaster)
        }

        if let splitViewController = viewController as? UISplitViewController {
            /// If the split is collapsed into a single view, only the first view controller is available.
            if let primaryViewController = splitViewController.viewControllers.first, splitViewController.isCollapsed {
                return topViewController(in: primaryViewController, prefersMaster: prefersMaster)
            }
            /// For non-collapsed scenarios, use the displayMode property to determine the view controller.
            switch splitViewController.displayMode {
            case .allVisible:
                /// Side-by-side display of primary and secondary.
                /// If prefersMaster is set to true, return the primary view, else return the detail view
                /// (default since the detail view usually occupies more screen space).
                if let primaryViewController = splitViewController.viewControllers.first, prefersMaster {
                    return topViewController(in: primaryViewController, prefersMaster: prefersMaster)
                }
                if let secondaryViewController = splitViewController.viewControllers.last {
                    return topViewController(in: secondaryViewController, prefersMaster: prefersMaster)
                }
            case .primaryHidden:
                /// Primary is hidden, return secondary.
                if let secondaryViewController = splitViewController.viewControllers.last {
                    return topViewController(in: secondaryViewController, prefersMaster: prefersMaster)
                }
            case .primaryOverlay:
                /// Primary is layered on top of secondary, return primary.
                if let primaryViewController = splitViewController.viewControllers.first {
                    return topViewController(in: primaryViewController, prefersMaster: prefersMaster)
                }
            /// Automatic is never reported according to the docs.
            case .automatic:
                break
            }
        }

        /// If the view controller is presenting, return the currently presented view controller.
        if let presentedViewController = viewController?.presentedViewController {
            return topViewController(in: presentedViewController, prefersMaster: prefersMaster)
        }

        return viewController
    }
    // swiftlint:enable cyclomatic_complexity

    static func dismissAll(completion: PresentationCompletion? = nil) {
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
        if let controller = rootViewController?.presentedViewController, !controller.isBeingDismissed {
            rootViewController?.dismiss(animated: false) {
                rootViewController?.setNeedsStatusBarAppearanceUpdate()
                completion?()
            }
        } else {
            rootViewController?.setNeedsStatusBarAppearanceUpdate()
            completion?()
        }
    }
}
