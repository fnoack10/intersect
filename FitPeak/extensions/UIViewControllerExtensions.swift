import UIKit

extension UIViewController {
    /// Presents a viewcontroller as popover on the iPad acnhored at sourceView.
    func presentUniversally(_ viewController: UIViewController, from sourceView: UIView, animated: Bool = true, completion: (() -> Void)? = nil) {
        if let presenter = viewController.popoverPresentationController {
            presenter.sourceView = sourceView
            presenter.sourceRect = sourceView.bounds
        }
        present(viewController, animated: animated, completion: completion)
    }

    /// Presents a viewcontroller as popover on the iPad acnhored at barButtonItem.
    func presentUniversally(_ viewController: UIViewController, fromBarButtonItem barButtonItem: UIBarButtonItem, animated: Bool = true) {
        if let presenter = viewController.popoverPresentationController {
            presenter.barButtonItem = barButtonItem
        }
        present(viewController, animated: animated, completion: nil)
    }
}
