import Foundation

import UIKit

extension Date {

    var iso8601: String { return DateFormatter.iso8601.string(from: self) }

    var iso8601Fractional: String { return DateFormatter.iso8601Fractional.string(from: self) }

    var iso8601OnlyDate: String { return DateFormatter.iso8601OnlyDate.string(from: self) }

    var iso8601FullDate: String { return DateFormatter.iso8601Full.string(from: self) }
    
    var fullDate: String { return DateFormatter.fullDate.string(from: self) }
    
    var standardDate: String { return DateFormatter.standardDate.string(from: self) }
    
    var shortDate: String { return DateFormatter.shortDate.string(from: self) }

    var month: String { return DateFormatter.month.string(from: self) }
    
    var day: String { return DateFormatter.day.string(from: self) }

}

// MARK: - Date checking
extension Date {
    
    func isInSameYear(date: Date) -> Bool {
        return Calendar.current.isDate(self, equalTo: date, toGranularity: .year)
    }

    func isCurrentYear() -> Bool {
        return self.isInSameYear(date: Date())
    }
    
    func translateDate() -> String {
        if Calendar.current.isDateInYesterday(self) { return "Yesterday" }
        else if Calendar.current.isDateInToday(self) { return "Today" }
        else { return self.fullDate }
    }
    
    func timeAgo() -> String {
        if #available(iOS 13.0, *) {
            let formatter = RelativeDateTimeFormatter()
            formatter.dateTimeStyle = .named
            formatter.unitsStyle = .full
            formatter.dateTimeStyle = .numeric
            formatter.locale = Locale(identifier: "en_US_POSIX") // .current
            return formatter.localizedString(for: self, relativeTo: Date())
        } else {
            var calendar = Calendar.current
            calendar.locale = Locale(identifier: "en_US_POSIX") // .current
            let formatter = DateComponentsFormatter()
            formatter.calendar = calendar
            formatter.allowedUnits = [.year, .month, .day, .hour, .minute]
            formatter.unitsStyle = .full
            formatter.zeroFormattingBehavior = .dropAll
            formatter.maximumUnitCount = 1
            guard let string = formatter.string(from: self, to: Date()) else { return ""}
            return string + " ago"
        }
    }

}
