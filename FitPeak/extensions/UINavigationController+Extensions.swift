import UIKit

enum NavigationItemType {
    case back
    case cancel
    case add
    
    var icon: UIImage {
        switch self {
        case .back: return UIImage(named: "back") ?? UIImage()
        case .cancel: return UIImage(named: "cancel") ?? UIImage()
        case .add: return UIImage(named: "plus") ?? UIImage()
        }
    }
}
