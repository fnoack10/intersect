import RxSwift

public protocol Optionable {
    associatedtype WrappedType
    func unwrap() -> WrappedType
    func isEmpty() -> Bool
}

extension Optional: Optionable {
    public typealias WrappedType = Wrapped
    public func unwrap() -> WrappedType {
        return self!
    }
    public func isEmpty() -> Bool {
        return self == nil
    }
}

/// See: https://github.com/RxSwiftCommunity/RxSwiftExt/blob/master/Source/unwrap.swift
extension ObservableType where E: Optionable {
    public func filterNil() -> Observable<E.WrappedType> {
        return self
            .filter { value in
                return !value.isEmpty()
            }
            .map { value -> E.WrappedType in
                value.unwrap()
            }
    }
}
