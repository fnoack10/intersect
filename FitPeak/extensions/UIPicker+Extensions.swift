import Foundation
import UIKit

extension UIPickerView {
    
    func setPickerLabels(labels: [Int: UILabel], containedView: UIView) {
        let labelWidth:CGFloat = containedView.bounds.width / CGFloat(self.numberOfComponents)
        let x:CGFloat = self.frame.origin.x
        let y:CGFloat = self.center.y
        
        for i in 0...self.numberOfComponents {
            
            if let label = labels[i] {
                
                if self.subviews.contains(label) {
                    label.removeFromSuperview()
                }
                
                label.frame.origin = CGPoint(x: x + labelWidth * CGFloat(i), y: y)
                label.font = UIFont(.semibold, 18)
                label.backgroundColor = .clear
                label.textAlignment = .left
                label.sizeToFit()

                self.addSubview(label)
                self.layoutIfNeeded()
            }
        }
    }
}
