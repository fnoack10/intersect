import Foundation

extension URL {

    static var mailApp: URL {
        return URL(string: "message:")!
    }

    /// Returns the part of the URL path at the given index.
    /// http://www.ottonova.de/otp/123456 would return "otp" at index 0.
    func pathComponent(at index: Int) -> String? {
        /// Path component at index 0 is "/"
        let componentIndex = index + 1
        guard pathComponents.count > componentIndex else { return nil }
        return pathComponents[componentIndex]
    }

    /// Return the value of the query parameter with the given name.
    func pathParam(_ name: String) -> String? {
        guard
            let urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true),
            let queryItem = urlComponents.queryItems?.filter({ $0.name == name }).first
            else { return nil }
        return queryItem.value
    }

    var isPhoneURL: Bool {
        guard let scheme = scheme, scheme == "tel" else { return false }
        return true
    }

}
