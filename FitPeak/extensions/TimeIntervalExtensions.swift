import Foundation
import UIKit

extension TimeInterval {
    
    var timeFromSeconds: String {
        guard let timeComponent = DateComponentsFormatter.timeFromSeconds.string(from: self) else { return "00:00" }
        return timeComponent
    }
    
}
