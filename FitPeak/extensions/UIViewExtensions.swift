import Foundation
import UIKit

extension UIView {
    
    func setGradientBackground(startColor: UIColor, endColor: UIColor) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.name = "gradient"
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func setGradientEnabled(_ enabled: Bool) {
        guard let layers = layer.sublayers else { return }
        for layer in layers {
            if layer.name == "gradient" {
                layer.isHidden = !enabled
            }
        }
    }
    
    func addDashedLine(color: UIColor = .lightGray) {
        backgroundColor = .clear
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.bounds = bounds
        shapeLayer.position = CGPoint(x: frame.width / 2, y: frame.height / 2)
        shapeLayer.fillColor = UIColor.blue.cgColor
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 2
        shapeLayer.lineJoin = .round
        shapeLayer.lineDashPattern = [3, 4]
        
        let path = CGMutablePath()
        path.move(to: CGPoint(x: frame.width/2, y: 0))
        path.addLine(to: CGPoint(x: frame.width/2, y: frame.height))
        shapeLayer.path = path
        
        layer.addSublayer(shapeLayer)
    }
    
}
