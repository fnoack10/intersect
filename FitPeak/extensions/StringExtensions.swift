import UIKit

extension String {
    // MARK: - Size Calculations

    /// Calculates the text height based on a constrained width and font.
    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect,
                                       options: .usesLineFragmentOrigin,
                                       attributes: [.font: font],
                                       context: nil)
        return boundingBox.height
    }

    // MARK: - Cases

    /// Capitalizes only the first letter.
    func capitalizedFirstLetter() -> String {
        let first = prefix(1).capitalized
        let other = dropFirst()
        return first + other
    }

    // MARK: - Hashing

    /// Returns the md5 hash as hex version.
//    func md5() -> String? {
//        guard let messageData = data(using: .utf8) else { return nil }
//        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
//        _ = digestData.withUnsafeMutableBytes { digestBytes in
//            messageData.withUnsafeBytes { messageBytes in
//                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
//            }
//        }
//        return digestData.map { String(format: "%02hhx", $0) }.joined()
//    }

    func matches(for regex: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: self, range: NSRange(self.startIndex..., in: self))
            return results.compactMap {
                Range($0.range, in: self).map { String(self[$0]) }
            }
        } catch {
            return []
        }
    }

    func isValidUrl() -> Bool {
        let urlRegEx = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
        return urlTest.evaluate(with: self)
    }

}

extension NSAttributedString {
    /// Calculates the text height based on a constrained width and font.
    func heightWithConstrainedWidth(_ width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            context: nil)
        return boundingBox.height
    }
}
