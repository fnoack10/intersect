import UIKit

extension UITableView {
    var canScroll: Bool { return contentSize.height > bounds.height }

    func scrollToBottom(animated: Bool = true, offset: Int = 0) {
        guard numberOfSections > 0 else { return }
        let numRows = numberOfRows(inSection: numberOfSections - 1)
        guard numRows > offset else { return }
        let indexPath = IndexPath(row: numRows - 1 - offset, section: numberOfSections - 1)
        scrollToRow(at: indexPath, at: .bottom, animated: animated)
    }
}
