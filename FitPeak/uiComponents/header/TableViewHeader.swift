import UIKit

class TableViewHeader: UIView {
    
    static func createPrimary(title: String, height: CGFloat, view: UIView) -> UIView {
        let headerView = UIView(frame: CGRect(x: 0, y:
            0, width: view.frame.width, height: height))
        headerView.backgroundColor = .backgroundColorLight
        
//        let topBorder = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 0.5))
//        topBorder.backgroundColor = .lineGray
//        headerView.addSubview(topBorder)
        
//        let bottomBorder = UIView(frame: CGRect(x: 0, y: height-0.5, width: view.frame.width, height: 0.5))
//        bottomBorder.backgroundColor = .lineGray
//        headerView.addSubview(bottomBorder)
        
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 15, width: view.frame.width, height: headerView.frame.height-15))
        headerLabel.text = title
        
        Theme.apply(primaryTableViewHeaderLabel: headerLabel)
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
        static func createSecondary(title: String, height: CGFloat, view: UIView) -> UIView {
            let headerView = UIView(frame: CGRect(x: 0, y:
                0, width: view.frame.width, height: height))
            headerView.backgroundColor = .backgroundColorLight
            
    //        let topBorder = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 0.5))
    //        topBorder.backgroundColor = .lineGray
    //        headerView.addSubview(topBorder)
            
    //        let bottomBorder = UIView(frame: CGRect(x: 0, y: height-0.5, width: view.frame.width, height: 0.5))
    //        bottomBorder.backgroundColor = .lineGray
    //        headerView.addSubview(bottomBorder)
            
            let headerLabel = UILabel(frame: CGRect(x: 20, y: 0, width: view.frame.width, height: headerView.frame.height))
            headerLabel.text = title
            
            Theme.apply(secondaryTableViewHeaderLabel: headerLabel)
            headerView.addSubview(headerLabel)
            
            return headerView
        }
    
}

