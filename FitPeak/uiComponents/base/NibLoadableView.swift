import UIKit

/// See: https://realm.io/news/appbuilders-natasha-muraschev-practical-protocol-oriented-programming/
protocol NibLoadableView: class {}

extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }

    static func fromNib(bundle: Bundle? = nil) -> Self {
        return UINib(nibName: nibName, bundle: bundle).instantiate(withOwner: self, options: nil).first! as! Self
    }
}
