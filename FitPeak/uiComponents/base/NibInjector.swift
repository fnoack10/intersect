/// See: https://vimeo.com/191470347

/*
 Usage example: Load Nib in init method of a custom UIView subclass:

 _ = NibInjector.inject(firstViewInNibNamed: "ExampleNib",
                        inBundle: Bundle(for: type(of: self)),
                        intoContainer: self,
                        withOwner: self)
*/

import UIKit

final class NibInjector {
    static func inject(firstViewInNibNamed name: String, inBundle bundle: Bundle?, intoContainer container: UIView, withOwner owner: Any?) -> UIView {
        return self.inject(firstViewInNib: UINib(nibName: name, bundle: bundle), intoContainer: container, withOwner: owner)
    }

    static func inject(firstViewInNib nib: UINib, intoContainer container: UIView, withOwner owner: Any?) -> UIView {
        let view = nib.instantiate(withOwner: owner, options: nil).first as! UIView
        ViewInjector.inject(view, into: container)
        return view
    }
}
