import UIKit

/// Helper class to add a view to a container and setup all layout constraints to pin to
/// the edges of the superview.
/// See: https://vimeo.com/191470347
final class ViewInjector {
    static func inject(_ view: UIView, into container: UIView, at index: Int? = nil) {
        view.translatesAutoresizingMaskIntoConstraints = false
        if let index = index {
            container.insertSubview(view, at: index)
        } else {
            container.addSubview(view)
        }
        view.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
    }

    static func inject(_ view: UIView, into container: UIViewController, at index: Int? = nil) {
        view.translatesAutoresizingMaskIntoConstraints = false
        if let index = index {
            container.view.insertSubview(view, at: index)
        } else {
            container.view.addSubview(view)
        }
        view.topAnchor.constraint(equalTo: container.topLayoutGuide.bottomAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: container.bottomLayoutGuide.topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: container.view.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: container.view.trailingAnchor).isActive = true
    }
}
