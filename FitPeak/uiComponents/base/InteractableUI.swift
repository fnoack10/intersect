import UIKit
import RxSwift

/// Base class for View Controllers. The init/creation methods accept a factory closure that returns an Interactor.
/// The UI passes itself as parameter to the closure so that action streams can be retrieved from the UI and
/// passed as dependency to the Interactor.
///
/// See: [DirectedViewController](https://github.com/ReactiveKit/ReactiveGitter/blob/master/Common/DirectedViewController.swift)
class InteractableUI<Interactor>: UIViewController {
    var interactor: Interactor!
    let disposeBag = DisposeBag()

    private var interactorFactory: ((InteractableUI) -> Interactor)!

    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, interactorFactory: @escaping (InteractableUI) -> Interactor) {
        self.interactorFactory = interactorFactory
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    @available(*, deprecated, message: "Views should be only created from SceneType")
    class func create(storyboard: UIStoryboard, interactorFactory: @escaping (InteractableUI) -> Interactor) -> InteractableUI {
        let viewController = storyboard.instantiateInitialViewController() as! InteractableUI
        viewController.interactorFactory = interactorFactory
        return viewController
    }

    class func create<T: InteractableUI>(scene: SceneType<T>, interactorFactory: @escaping (InteractableUI) -> Interactor) -> T {
        let viewController = scene.instantiate()
        viewController.interactorFactory = interactorFactory
        return viewController
    }

    @available(*, deprecated, message: "Views should be only created from SceneType")
    class func create(storyboard: UIStoryboard, identifier: String, interactorFactory: @escaping (InteractableUI) -> Interactor) -> InteractableUI {
        let viewController = storyboard.instantiateViewController(withIdentifier: identifier) as! InteractableUI
        viewController.interactorFactory = interactorFactory
        return viewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        loadInteractorIfNeeded()
    }

    func loadInteractorIfNeeded() {
        guard interactor == nil else { return }
        interactor = interactorFactory(self)
        interactorFactory = nil
        bindInteractor(interactor: interactor)
    }

    func bindInteractor(interactor: Interactor) {}

    class func downcast<T, U, D>(_ closure: @escaping (T) -> D) -> ((U) -> D) {
        return { param in closure(param as! T) }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}
