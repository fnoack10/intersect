import Foundation

enum NavigationMode {
    // default shows the standard back button
    // none hides the the standard back button
    // back shows custom back button
    // cancel shows a cancel button
    case `default`, none, back, cancel
}

enum NavigationStyle {
    case dark, light
}

protocol NavigationModePM {
    var navigationMode: NavigationMode { get }
    var navigationStyle: NavigationStyle { get }
}

extension NavigationModePM {

    var navigationStyle: NavigationStyle {
        return .dark
    }

}
