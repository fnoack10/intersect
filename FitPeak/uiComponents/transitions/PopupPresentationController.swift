import UIKit

/// Custom presentation controller for modal popups on the iPad.
final class PopupPresentationController: UIPresentationController {
    var preferredSize: CGSize?
    var didTapOutside: (() -> Void)?

    private var dimmingView: UIView!

    // MARK: - Lifecycle

    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)

        setupDimmingView()
    }

    // MARK: - Dimming Views

    private func setupDimmingView() {
        dimmingView = UIView()
        dimmingView.translatesAutoresizingMaskIntoConstraints = false
        dimmingView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        dimmingView.alpha = 0.0

        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
        dimmingView.addGestureRecognizer(tapRecognizer)
    }

    @objc private func handleTap(recognizer: UITapGestureRecognizer) {
        if let didTapOutside = didTapOutside {
            didTapOutside()
        } else {
            presentingViewController.dismiss(animated: true)
        }
    }

    // MARK: - Presentation Transitions

    override func presentationTransitionWillBegin() {
        guard let containerView = containerView else { return }

        presentedView?.layer.cornerRadius = 8.0
        presentedView?.clipsToBounds = true

        ViewInjector.inject(dimmingView, into: containerView, at: 0)

        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 1.0
            return
        }
        coordinator.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 1.0
        })
    }

    override func dismissalTransitionWillBegin() {
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 0.0
            return
        }
        coordinator.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 0.0
        })
    }

    // MARK: - Layout

    override func containerViewWillLayoutSubviews() {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }

    /// Size of content.
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        return preferredSize ?? CGSize(width: parentSize.width * 0.5, height: parentSize.height * (2.0 / 3.0))
    }

    override var frameOfPresentedViewInContainerView: CGRect {
        var frame: CGRect = .zero
        let containerBounds = containerView!.bounds
        frame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerBounds.size)
        frame.origin.x = containerBounds.width * 0.5 - frame.size.width * 0.5
        frame.origin.y = containerBounds.height * 0.5 - frame.size.height * 0.5
        return frame.integral
    }
}
