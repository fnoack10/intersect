import UIKit

/// Transition Delegate setting up a custom popup presentation controller.
final class PopupTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate, UIAdaptivePresentationControllerDelegate {
    var preferredSize: CGSize?
    var didTapOutside: (() -> Void)?
    var isAdaptive = true

    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let presentationController = PopupPresentationController(presentedViewController: presented, presenting: presenting)
        presentationController.preferredSize = preferredSize
        presentationController.didTapOutside = didTapOutside
        presentationController.delegate = self
        return presentationController
    }

    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        if traitCollection.horizontalSizeClass == .compact {
            return isAdaptive ? .overFullScreen : .none
        } else {
            return .none
        }
    }
}
