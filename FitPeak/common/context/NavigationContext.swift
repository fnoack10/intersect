import UIKit

/// Use custom NavigationController class for appearance proxies (see Theme class).
class NavigationController: UINavigationController {}

/// Presentation Context for stack-like push/pop navigation.
/// Internally uses UINavigationController.
/// See: https://github.com/ReactiveKit/ReactiveGitter/blob/master/Common/NavigationContext.swift
protocol NavigationContext: Context {
    func push(_ userInterface: UIViewController, animated: Bool, mode: PresentationMode, completion: PresentationCompletion?)
    func pop(animated: Bool)
    func popToRoot(animated: Bool)
}

final class NavigationControllerContext: NavigationContext {
    let parent: Context
    let configure: ((UINavigationController) -> Void)?

    weak var navigationController: UINavigationController!

    private lazy var popupTransitioning = PopupTransitioningDelegate()

    init(parent: Context, configure: ((UINavigationController) -> Void)? = nil) {
        self.parent = parent
        self.configure = configure
    }

    func push(_ userInterface: UIViewController, animated: Bool = true, mode: PresentationMode = .default, completion: PresentationCompletion? = nil) {
        if let navigationController = navigationController {
            navigationController.pushViewController(userInterface, animated: animated)
        } else {
            let navigationController = NavigationController(rootViewController: userInterface)
            configure?(navigationController)
            parent.present(navigationController, animated: animated, mode: mode, completion: completion)
            self.navigationController = navigationController
        }
    }

    func pop(animated: Bool = true) {
        navigationController.popViewController(animated: animated)
    }

    func popToRoot(animated: Bool = true) {
        navigationController.popToRootViewController(animated: animated)
    }

    func popToViewController(ofClass viewControllerClass: AnyClass, animated: Bool = true) {
        for viewController in self.navigationController.viewControllers {
            if viewController.isKind(of: viewControllerClass) {
                navigationController.popToViewController(viewController, animated: animated)
            }
        }
    }

    func present(_ userInterface: UIViewController, animated: Bool = true, mode: PresentationMode = .default, completion: PresentationCompletion? = nil) {
        if let navigationController = navigationController {
            switch mode {
            case .default: navigationController.present(userInterface, animated: animated, completion: completion)
            case .topmost: UIApplication.topViewController()?.present(userInterface, animated: animated, completion: completion)
            }
        } else {
            parent.present(userInterface, animated: animated, mode: mode, completion: completion)
        }
    }

    func dismiss(animated: Bool, completion: PresentationCompletion?) {
        navigationController.dismiss(animated: animated, completion: completion)
    }

    // MARK: - Detail Presentation & Dismissal

    func presentDetail(_ userInterface: UIViewController, preferredSize: CGSize = CGSize(width: 600, height: 620), didDismiss: @escaping () -> Void) {
        if UIDevice.current.userInterfaceIdiom == .pad {
            presentAsPopup(userInterface, preferredSize: preferredSize, didDismiss: didDismiss)
        } else {
            push(userInterface)
        }
    }

    /// On the iPad, present the detail view in a popup.
    private func presentAsPopup(_ userInterface: UIViewController, preferredSize: CGSize, didDismiss: @escaping () -> Void) {
        let detailNavigationController = NavigationController(rootViewController: userInterface)
        detailNavigationController.modalPresentationStyle = .custom

        popupTransitioning.preferredSize = preferredSize
        popupTransitioning.didTapOutside = didDismiss

        detailNavigationController.transitioningDelegate = popupTransitioning
        present(detailNavigationController)
    }

    func dismissDetail(animated: Bool = true) {
        if navigationController.traitCollection.userInterfaceIdiom == .pad {
            dismiss(animated: animated, completion: nil)
        } else {
            popToRoot()
        }
    }
}
