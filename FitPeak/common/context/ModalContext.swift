import UIKit

/// Presentation Context for modal presentation.
/// See: https://github.com/ReactiveKit/ReactiveGitter/blob/master/Common/ModalContext.swift
final class ModalContext: Context {
    let presenter: UIViewController

    private lazy var popupTransitioning = PopupTransitioningDelegate()

    init(presenter: UIViewController) {
        self.presenter = presenter
    }

    func present(_ userInterface: UIViewController, animated: Bool = true, mode: PresentationMode = .default, completion: PresentationCompletion? = nil) {
        switch mode {
        case .default:
            if let _ = presenter.presentedViewController {
                presenter.dismiss(animated: false)
                DispatchQueue.main.async {
                    self.presenter.present(userInterface, animated: animated, completion: completion)
                }
            } else {
                presenter.present(userInterface, animated: animated, completion: completion)
            }
        case .topmost:
            UIApplication.topViewController()?.present(userInterface, animated: animated, completion: completion)
        }
    }

    func presentPopup(_ userInterface: UIViewController, preferredSize: CGSize, didDismiss: (() -> Void)? = nil) {
        userInterface.modalPresentationStyle = .custom

        popupTransitioning.isAdaptive = false
        popupTransitioning.preferredSize = preferredSize
        popupTransitioning.didTapOutside = didDismiss

        userInterface.transitioningDelegate = popupTransitioning
        present(userInterface)
    }

    func dismiss(animated: Bool = true, completion: PresentationCompletion? = nil) {
        presenter.dismiss(animated: animated, completion: completion)
    }
}
