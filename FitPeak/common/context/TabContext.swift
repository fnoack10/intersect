import UIKit

final class TabContext: Context {
    let parent: Context
    var userInterfaces = [UIViewController]()
    weak var tabBarController: UITabBarController!
    var selectedIndex: Int { return tabBarController.selectedIndex }

    init(parent: Context) {
        self.parent = parent
    }

    func present(animated: Bool = true, mode: PresentationMode = .default, completion: PresentationCompletion? = nil) {
        guard let _ = tabBarController else {
            let tabBarController = UITabBarController()
            tabBarController.tabBar.isTranslucent = false
            tabBarController.tabBar.backgroundColor = .backgroundColorPrimary
            tabBarController.viewControllers = userInterfaces
            self.tabBarController = tabBarController
            parent.present(tabBarController, animated: animated, mode: mode, completion: completion)
            return
        }
    }

    func dismiss(animated: Bool = true, completion: PresentationCompletion? = nil) {
        let viewController = tabBarController.presentedViewController ?? tabBarController
        viewController?.dismiss(animated: animated) {
            self.tabBarController.setNeedsStatusBarAppearanceUpdate()
            completion?()
        }
    }

    func present(_ userInterface: UIViewController, animated: Bool = true, mode: PresentationMode = .default, completion: PresentationCompletion? = nil) {
        if let tabBarController = tabBarController {
            switch mode {
            case .default: tabBarController.present(userInterface, animated: animated, completion: completion)
            case .topmost: UIApplication.topViewController()?.present(userInterface, animated: animated, completion: completion)
            }
        } else {
            userInterfaces.append(userInterface)
        }
    }

    func selectTab(at index: Int) {
        guard let tabBarController = tabBarController else { return }
        tabBarController.selectedIndex = index
    }
}
