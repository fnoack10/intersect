import UIKit

/// Top-most Presentation Context.
/// See: https://github.com/ReactiveKit/ReactiveGitter/blob/master/Common/WindowContext.swift
final class WindowContext: Context {
    let window: UIWindow

    init(window: UIWindow) {
        self.window = window
    }

    /// Attaches the UI as new rootViewController to the Window if needed
    /// and animates transition if UI replaces an existing rootViewController.
    func present(_ userInterface: UIViewController, animated: Bool = true, mode: PresentationMode = .default, completion: PresentationCompletion? = nil) {
        if window.rootViewController == nil || !animated {
            window.rootViewController = userInterface
            completion?()
        } else {
            let screenshot = window.rootViewController?.view.snapshotView(afterScreenUpdates: false) ?? UIView()
            window.rootViewController = userInterface
            userInterface.view.addSubview(screenshot)

            UIView.animate(withDuration: 0.125, animations: {
                screenshot.alpha = 0
            }, completion: { _ in
                screenshot.removeFromSuperview()
                completion?()
            })
        }
    }

    func dismiss(animated: Bool, completion: PresentationCompletion?) {
        window.rootViewController?.dismiss(animated: false, completion: completion)
    }
}
