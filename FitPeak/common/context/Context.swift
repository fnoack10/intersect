import UIKit

/// Base protocol for a Presentation Context.
protocol Context: class {
    func present(_ userInterface: UIViewController, animated: Bool, mode: PresentationMode, completion: PresentationCompletion?)
    func dismiss(animated: Bool, completion: PresentationCompletion?)
}

extension Context {
    func dismiss(animated: Bool = true, completion: PresentationCompletion? = nil) {
        completion?()
    }
}

typealias PresentationCompletion = () -> Void

enum PresentationMode {
    case `default`
    case topmost
}
