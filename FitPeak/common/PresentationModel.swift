import Foundation

// MARK: - PresentationModel

protocol HasEmptyState {
   var isEmpty: Bool { get }
}

/// Protocol for Presentation Models with state.
protocol PresentationModel: HasEmptyState {
    var presentationState: PresentationState { get set }

    init()
}

extension PresentationModel {
    var isEmpty: Bool { return false }

    init(_ presentationState: PresentationState) {
        self.init()
        self.presentationState = presentationState
    }

    static func success() -> Self {
        return self.init(.success)
    }

    static func loading() -> Self {
        return self.init(.loading)
    }

    static func failure(_ error: Error) -> Self {
        return self.init(.failure(error))
    }

    mutating func success() -> Self {
        presentationState = .success
        return self
    }

    mutating func loading() -> Self {
        presentationState = .loading
        return self
    }

    mutating func failure(_ error: Error) -> Self {
        presentationState = .failure(error)
        return self
    }

    var isLoading: Bool { return presentationState.isLoading }
    var isSuccess: Bool { return presentationState.isSuccess }
    var isFailure: Bool { return presentationState.isFailure }
}

// MARK: - PresentationState

/// Provides states for loading, success, and failure cases.
enum PresentationState {
    case loading
    case success
    case failure(Error)

    var isLoading: Bool {
        switch self {
        case .loading: return true
        default: return false
        }
    }

    var isSuccess: Bool {
        switch self {
        case .success: return true
        default: return false
        }
    }

    var isFailure: Bool {
        switch self {
        case .failure: return true
        default: return false
        }
    }

    var errorMessage: String? {
        switch self {
        case let .failure(error): return error.localizedDescription
        default: return nil
        }
    }
}

// MARK: - Equality

func == (lhs: PresentationState, rhs: PresentationState) -> Bool {
    switch (lhs, rhs) {
    case (.loading, .loading): return true
    case (.success, .success): return true
    case (.failure(let failureA), .failure(let failureB)):
        if failureA.localizedDescription != failureB.localizedDescription { return false }
        return true
    default: return false
    }
}

func != (lhs: PresentationState, rhs: PresentationState) -> Bool {
    return !(lhs == rhs)
}
