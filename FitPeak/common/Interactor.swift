import Foundation
import RxSwift

protocol InteractorInputs {}

protocol InteractorOutputs {

    var didFinish: PublishSubject<Void> { get }

}

protocol InteractorUIOutputs {}

protocol Interactor: InteractorUIOutputs, InteractorInputs, InteractorOutputs {

    var disposeBag: DisposeBag { get }

}
