import Foundation
import RxSwift

protocol Scene: class {
    var children: [Scene] { get set }
    var disposeBag: DisposeBag { get set }
    func start()
}

extension Scene {

    func addChild(_ scene: Scene) {
        if !children.contains(where: { $0 === scene }) {
            self.children.append(scene)
        }
    }

    func removeChild(_ scene: Scene) {
        self.children = self.children.filter { $0 !== scene }
    }

    func removeAll(_ scene: Scene.Type) {
        self.children = self.children.filter { type(of: $0) != scene }
    }

    func removeAllChildren() {
        self.children.removeAll()
    }

    func popChild() {
        self.children.removeLast()
    }

}
