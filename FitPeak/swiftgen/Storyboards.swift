// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Storyboard Scenes

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
internal enum StoryboardScene {
  internal enum Feedback: StoryboardType {
    internal static let storyboardName = "Feedback"

    internal static let feedbackUI = SceneType<FitPeak.FeedbackUI>(storyboard: Feedback.self, identifier: "FeedbackUI")
  }
  internal enum Input: StoryboardType {
    internal static let storyboardName = "Input"

    internal static let inputUI = SceneType<FitPeak.InputUI>(storyboard: Input.self, identifier: "InputUI")
  }
  internal enum Login: StoryboardType {
    internal static let storyboardName = "Login"

    internal static let loginUI = SceneType<FitPeak.LoginUI>(storyboard: Login.self, identifier: "LoginUI")
  }
  internal enum MovementOverview: StoryboardType {
    internal static let storyboardName = "MovementOverview"

    internal static let movementOverviewUI = SceneType<FitPeak.MovementOverviewUI>(storyboard: MovementOverview.self, identifier: "MovementOverviewUI")
  }
  internal enum Overview: StoryboardType {
    internal static let storyboardName = "Overview"

    internal static let overviewUI = SceneType<FitPeak.OverviewUI>(storyboard: Overview.self, identifier: "OverviewUI")
  }
  internal enum PersonalInformation: StoryboardType {
    internal static let storyboardName = "PersonalInformation"

    internal static let personalInformationUI = SceneType<FitPeak.PersonalInformationUI>(storyboard: PersonalInformation.self, identifier: "PersonalInformationUI")
  }
  internal enum Profile: StoryboardType {
    internal static let storyboardName = "Profile"

    internal static let profileUI = SceneType<FitPeak.ProfileUI>(storyboard: Profile.self, identifier: "ProfileUI")
  }
  internal enum Signup: StoryboardType {
    internal static let storyboardName = "Signup"

    internal static let measurementUI = SceneType<FitPeak.MeasurementUI>(storyboard: Signup.self, identifier: "MeasurementUI")

    internal static let signUpUI = SceneType<FitPeak.SignUpUI>(storyboard: Signup.self, identifier: "SignUpUI")
  }
  internal enum Welcome: StoryboardType {
    internal static let storyboardName = "Welcome"
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

// MARK: - Implementation Details

internal protocol StoryboardType {
  static var storyboardName: String { get }
}

internal extension StoryboardType {
  static var storyboard: UIStoryboard {
    let name = self.storyboardName
    return UIStoryboard(name: name, bundle: Bundle(for: BundleToken.self))
  }
}

internal struct SceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type
  internal let identifier: String

  internal func instantiate() -> T {
    let identifier = self.identifier
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

internal struct InitialSceneType<T: UIViewController> {
  internal let storyboard: StoryboardType.Type

  internal func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

private final class BundleToken {}
