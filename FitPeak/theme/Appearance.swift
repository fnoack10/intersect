import UIKit

final class Appearance {
    
    class func tabBar() {
        UITabBar.appearance().tintColor = .tabBarIconColor
       // UITabBar.appearance().unselectedItemTintColor = .tabBarIconColor
        
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: 2.0)
        UITabBarItem.appearance().setTitleTextAttributes([.font: UIFont(.medium, 12)], for: .normal)
    }
    
}
