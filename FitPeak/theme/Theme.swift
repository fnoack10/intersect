import Foundation
import UIKit

protocol ThemeProtocol {
    static func apply(_ navigationController: UINavigationController?, tintColor: UIColor)
    static func apply(_ viewController: UIViewController)
    static func apply(headerTitleLabel: UILabel)
    static func apply(primaryTableViewHeaderLabel: UILabel)
    static func apply(secondaryTableViewHeaderLabel: UILabel)
    static func apply(_ button: UIButton)
    static func apply(_ label: UILabel)
    static func apply(_ textView: UITextView)
}

extension ThemeProtocol {

    static func apply(_ navigationController: UINavigationController?, tintColor: UIColor) {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.titleTextAttributes = [.font: UIFont(.bold, 17),
                                             .foregroundColor: tintColor]
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.largeTitleTextAttributes = [.font: UIFont(.bold, 30),
                                                                            .foregroundColor: tintColor]
        }
    }
    
    static func apply(_ viewController: UIViewController) {
        viewController.view.backgroundColor = .backgroundColorPrimary
    }
    
    static func apply(headerTitleLabel: UILabel) {
        headerTitleLabel.textColor = .primaryTextColor
        headerTitleLabel.font = UIFont(.bold, 30)
    }
    
    static func apply(primaryTableViewHeaderLabel: UILabel) {
        primaryTableViewHeaderLabel.font = UIFont(.bold, 20)
        primaryTableViewHeaderLabel.textColor = .primaryTextColor
    }
    
    static func apply(secondaryTableViewHeaderLabel: UILabel) {
        secondaryTableViewHeaderLabel.font = UIFont(.bold, 16)
        secondaryTableViewHeaderLabel.textColor = .primaryTextColor
    }
    
    static func apply(_ button: UIButton) {
        button.titleLabel?.font = UIFont(.bold, 16)
        button.setTitleColor(.lightTextColor, for: .normal)
        button.setBackgroundColor(color: .activeButtonColor, forState: .normal)
        button.setBackgroundColor(color: .disabledButtonColor, forState: .disabled)
        button.layer.cornerRadius = 12.0
        button.clipsToBounds = true
    }
    
    static func apply(_ label: UILabel) {
        label.textColor = .primaryTextColor
        label.font = UIFont(.regular, 16)
    }
    
    static func apply(_ textView: UITextView) {
        textView.textColor = .primaryTextColor
        textView.font = UIFont(.regular, 16)
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = UIColor.lineGray.cgColor
        textView.layer.cornerRadius = 8.0
        textView.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
    
}

struct Theme: ThemeProtocol {
    
    // TableView Cell
    static func apply(_ cell: InputTableViewCell) {
        cell.clipsToBounds = false
        cell.inputField.font = UIFont(.regular, 16)
        cell.inputField.textColor = .black
        cell.detailLabel.font = UIFont(.regular, 10)
        cell.detailLabel.textColor = .lightGray
        cell.iconImage.tintColor = .inactiveInputColor
        cell.separationView.backgroundColor = .separationLine
    }
    
    static func apply(_ cell: OverviewTableViewCell) {
        cell.titleLabel.textColor = .black
        cell.titleLabel.font = UIFont(.regular, 16)
        cell.subtitleLabel.textColor = .gray
        cell.subtitleLabel.font = UIFont(.regular, 10)
    }
    
    static func apply(_ cell: LogTableViewCell) {
        cell.titleLabel.textColor = .primaryTextColor
        cell.titleLabel.font = UIFont(.semibold, 18)
        cell.dateLabel.textColor = .detailTextColor
        cell.dateLabel.font = UIFont(.semibold, 12)
        cell.outerPointView.layer.cornerRadius = cell.outerPointView.frame.height/2
        cell.innerPointView.layer.cornerRadius = cell.innerPointView.frame.height/2
    }
    
    static func apply(titleLabel: UILabel) {
        titleLabel.textColor = .primaryTextColor
        titleLabel.font = UIFont(.bold, 20)
    }
    
    static func apply(cardText: UILabel) {
        cardText.textColor = .white
        cardText.font = UIFont(.bold, 16)
    }
    
    static func apply(backButton: UIButton) {
        let image = UIImage(named: "back")?.withRenderingMode(.alwaysTemplate)
        backButton.setImage(image, for: .normal)
        backButton.tintColor = .black
    }
    
}

extension UIFont {
    enum Font: String {
        case regular = "Avenir-Roman"
        case medium = "Avenir-Medium"
        case semibold = "Avenir-Heavy"
        case bold = "Avenir-Black"
        
        static func printAll() {
            let fontFamilyNames = UIFont.familyNames
            for familyName in fontFamilyNames {
                print("------------------------------")
                print("Font Family Name: \(familyName)")
                let names = UIFont.fontNames(forFamilyName: familyName)
                print("Font Names: \(names)")
            }
        }
    }
}

extension UIColor {
    
    // Palette
    static var main = UIColor(hex: 0x0070FF)
    static var mainLight = UIColor(hex: 0xFFFFFF, alpha: 0.8)
    
    // Background
    static var backgroundColorPrimary = UIColor.white
    static var backgroundColorSecondary = UIColor(hex: 0xF7F8FD)
    static var backgroundColorLight = UIColor.mainLight
    
    // NavigationBar
    static var navigationTintDark = UIColor.black
    static var navigationTintLight = UIColor.white
    
    // Text
    static var primaryTextColor = UIColor.black
    static var secondaryTextColor = UIColor.gray
    static var detailTextColor = UIColor.lightGray
    static var lightTextColor = UIColor.white
    static var activeTextColor = UIColor.main
    static var errorTextColor = UIColor.red
    
    // Icon
    static var tabBarIconColor = UIColor.black
    
    // Inactive
    static var inactiveGray = UIColor(hex: 0xCBCBCB)
    
    static var lineGray = UIColor(hex: 0xDEDFDE)
    static var activeButtonColor = UIColor.main
    static var disabledButtonColor = UIColor.lightGray
    static var cancelButtonColor = UIColor.lightGray
    
    static var activeInputColor = UIColor.main
    static var inactiveInputColor = UIColor.lightGray
    
    static var separationLine = UIColor.lightGray
    
    
    static func hex(_ hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension UIFont {
    convenience init!(_ font: Font, _ size: CGFloat) {
        self.init(name: font.rawValue, size: size)
    }
    
    convenience init!(_ font: Font, _ style: UIFont.TextStyle) {
        let preferredFont = UIFont.preferredFont(forTextStyle: style)
        self.init(name: font.rawValue, size: preferredFont.pointSize)
    }
}
